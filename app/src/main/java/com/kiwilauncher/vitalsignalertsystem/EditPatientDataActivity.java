package com.kiwilauncher.vitalsignalertsystem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.TimeZone;


public class EditPatientDataActivity extends Activity {
    private ImageButton headerBackBt;
    private Button saveBt;
    private EditText pEt, rEt, bpEt, spo2Et;
    private EditText tEt, oEt, vEt, uEt, sEt;

    private ModelController modelController;
    private SimpleDateFormat dateFormat;

    private BuildInputTask buildInputTask;

    private PatientData patient_data;

    int p, r, bp, bpLower, spo2;
    String t;
    String o, v, u, s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_patient_data);


        // --------------------------Set Header functions--------------------------
        TextView headerT = (TextView) findViewById(R.id.header_t_title);
        headerT.setText("แก้ไขข้อมูลผู้ป่วย");

        headerBackBt = (ImageButton) findViewById(R.id.header_imgbt_back);
        headerBackBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setResult(0);
                finish();
            }
        });


        // --------------------------Set Models & Controllers--------------------------
        modelController = new ModelController(getApplicationContext());
        modelController.open();

        dateFormat = new SimpleDateFormat(Patient.DATE_FORMAT);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));

        pEt = (EditText) findViewById(R.id.edit_patient_data_et_p);
        rEt = (EditText) findViewById(R.id.edit_patient_data_et_r);
        bpEt = (EditText) findViewById(R.id.edit_patient_data_et_bp);
        spo2Et = (EditText) findViewById(R.id.edit_patient_data_et_spo2);

        tEt = (EditText) findViewById(R.id.edit_patient_data_et_t);
        oEt = (EditText) findViewById(R.id.edit_patient_data_et_o);
        vEt = (EditText) findViewById(R.id.edit_patient_data_et_v);
        uEt = (EditText) findViewById(R.id.edit_patient_data_et_u);
        sEt = (EditText) findViewById(R.id.edit_patient_data_et_s);

        saveBt = (Button) findViewById(R.id.edit_patient_data_bt_save);
        saveBt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                // validate inputs
                if (validateInput()) {
                    saveBt.setEnabled(false);
                    saveBt.setText("กำลังบันทึกข้อมูล...");
                    saveBt.setTextColor(getResources().getColor(R.color.halfup_gray));

                    p = Integer.parseInt(pEt.getText().toString());
                    r = Integer.parseInt(rEt.getText().toString());
                    String[] bpSet = bpEt.getText().toString().split("/");
                    bp = Integer.parseInt(bpSet[0]);
                    bpLower = Integer.parseInt(bpSet[1]);
                    spo2 = Integer.parseInt(spo2Et.getText().toString());

                    String tStr = tEt.getText().toString();
                    String oStr = oEt.getText().toString();
                    String vStr = vEt.getText().toString();
                    String uStr = uEt.getText().toString();
                    String sStr = sEt.getText().toString();

                    if (!tStr.trim().isEmpty()) {
                        t = tStr;
                    }
                    if (!oStr.trim().isEmpty()) {
                        o = oStr;
                    }
                    if (!vStr.trim().isEmpty()) {
                        v = vStr;
                    }
                    if (!uStr.trim().isEmpty()) {
                        u = uStr;
                    }
                    if (!sStr.trim().isEmpty()) {
                        s = sStr;
                    }

                    new SaveDataTask().execute();
                }
                else {
                    // display error
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(EditPatientDataActivity.this);
                    alertBuilder.setMessage("โปรดใส่ข้อมูลให้ถูกต้องและครบถ้วน");
                    alertBuilder.setNeutralButton("OK", null);
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                }
            }
        });


        // --------------------------Set Data from AsyncTask--------------------------
        buildInputTask = new BuildInputTask();
        buildInputTask.execute();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        modelController.close();

        if (buildInputTask.getStatus() == AsyncTask.Status.RUNNING) {
            buildInputTask.cancel(true);
        }

        super.onDestroy();
    }


    @SuppressLint("SimpleDateFormat")
    private boolean validateInput() {
        boolean isValidated = true;

        String p = pEt.getText().toString();
        String r = rEt.getText().toString();
        String bp = bpEt.getText().toString();
        String spo2 = spo2Et.getText().toString();

        if (p.trim().isEmpty() || r.trim().isEmpty() || bp.trim().isEmpty() || spo2.trim().isEmpty()) {
            isValidated = false;
            Log.e("Valid not pass", "score factor");
        }
        else if (!bp.matches("(\\d+)/(\\d+)")) {
            isValidated = false;
            Log.e("Valid not pass", "BP format");
        }

        return isValidated;
    }


    private class BuildInputTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... params) {
            patient_data = modelController.getPatientData(getIntent().getLongExtra("patient_data_id", 0));

            return 1;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            // Set score data
            pEt.setText("" + patient_data.getP());
            rEt.setText("" + patient_data.getR());
            bpEt.setText("" + patient_data.getBp() + "/" + patient_data.getBpLower());
            spo2Et.setText("" + patient_data.getSpo2());

            // Set other data
            tEt.setText("" + ((patient_data.getT() != null) ? patient_data.getT() : ""));
            oEt.setText("" + ((patient_data.getO() != null) ? patient_data.getO() : ""));
            vEt.setText("" + ((patient_data.getV() != null) ? patient_data.getV() : ""));
            uEt.setText("" + ((patient_data.getU() != null) ? patient_data.getU() : ""));
            sEt.setText("" + ((patient_data.getS() != null) ? patient_data.getS() : ""));

            super.onPostExecute(integer);
        }
    }

    private class SaveDataTask extends AsyncTask<Void, Void, Integer> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            int currentOrientation = getResources().getConfiguration().orientation;
            if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            }
            else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }

            dialog = new ProgressDialog(EditPatientDataActivity.this);
            dialog.setMessage("กำลังบันทึกข้อมูล...");
            dialog.setCancelable(false);
            dialog.show();

            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            patient_data.setP(p);
            patient_data.setR(r);
            patient_data.setBp(bp);
            patient_data.setSpo2(spo2);
            patient_data.calculateScore();

            patient_data.setOtherData(t, o, v, u, s);

            patient_data.setUpdatedAt(new GregorianCalendar());

            modelController.updatePatientData(patient_data);

            return 1;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            dialog.dismiss();

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

            setResult(1);
            finish();

            super.onPostExecute(integer);
        }
    }
}
