package com.kiwilauncher.vitalsignalertsystem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class InputActivity extends Activity {
	Button saveBt;
	ImageButton headerBackBt;
	AutoCompleteTextView nameEt;
	EditText roomEt, typeEt, hnEt, birthdayEt;
	EditText pEt, rEt, bpEt, spo2Et;
	EditText tEt, oEt, vEt, uEt, sEt;
	
	PatientAutocompleteAdapter adapter;
	
	ModelController modelController;
	PatientData patientData;
	SimpleDateFormat dateFormat;
	
	String name, room, type, hn, birthdayStr, birthdayStrThai;
	int p, r, bp, bpLower, spo2;
	String t;
	String o, v, u, s;
	

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_input);
		
		modelController = new ModelController(getApplicationContext());
		modelController.open();
		
		dateFormat = new SimpleDateFormat(Patient.DATE_FORMAT);
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
		
		TextView headerT = (TextView) findViewById(R.id.header_t_title);
		headerT.setText("ใส่ข้อมูลผู้ป่วย");
		
		saveBt = (Button) findViewById(R.id.input_bt_save);
		saveBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// validate inputs
				if (validateInput()) {
					// get inputs
					saveBt.setEnabled(false);
					saveBt.setText("กำลังบันทึกข้อมูล...");
					saveBt.setTextColor(getResources().getColor(R.color.halfup_gray));
					
					name = nameEt.getText().toString();
					room = roomEt.getText().toString();
					type = typeEt.getText().toString();
					hn = hnEt.getText().toString();
					birthdayStrThai = birthdayEt.getText().toString();
					birthdayStr = DateHelper.fromThaiStr(birthdayStrThai);
					
					p = Integer.parseInt(pEt.getText().toString());
					r = Integer.parseInt(rEt.getText().toString());
					String[] bpSet = bpEt.getText().toString().split("/");
					bp = Integer.parseInt(bpSet[0]);
					bpLower = Integer.parseInt(bpSet[1]);
					spo2 = Integer.parseInt(spo2Et.getText().toString());
					
					String tStr = tEt.getText().toString();
					String oStr = oEt.getText().toString();
					String vStr = vEt.getText().toString();
					String uStr = uEt.getText().toString();
					String sStr = sEt.getText().toString();
					
					if (!tStr.trim().isEmpty()) {
						t = tStr;
					}
					if (!oStr.trim().isEmpty()) {
						o = oStr;
					}
					if (!vStr.trim().isEmpty()) {
						v = vStr;
					}
					if (!uStr.trim().isEmpty()) {
						u = uStr;
					}
					if (!sStr.trim().isEmpty()) {
						s = sStr;
					}
					
					new SaveDataTask().execute();
				}
				else {
					// display error
					AlertDialog.Builder alertBuilder = new AlertDialog.Builder(InputActivity.this);
					alertBuilder.setMessage("โปรดใส่ข้อมูลให้ถูกต้องและครบถ้วน");
					alertBuilder.setNeutralButton("OK", null);
					AlertDialog alert = alertBuilder.create();
					alert.show();
				}
			}
		});
		
		headerBackBt = (ImageButton) findViewById(R.id.header_imgbt_back);
		headerBackBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		nameEt = (AutoCompleteTextView) findViewById(R.id.input_et_name);
		roomEt = (EditText) findViewById(R.id.input_et_room);
		typeEt = (EditText) findViewById(R.id.input_et_type);
		hnEt = (EditText) findViewById(R.id.input_et_hn);
		birthdayEt = (EditText) findViewById(R.id.input_et_birthday);
		
		pEt = (EditText) findViewById(R.id.input_et_p);
		rEt = (EditText) findViewById(R.id.input_et_r);
		bpEt = (EditText) findViewById(R.id.input_et_bp);
		spo2Et = (EditText) findViewById(R.id.input_et_spo2);
		
		tEt = (EditText) findViewById(R.id.input_et_t);
		oEt = (EditText) findViewById(R.id.input_et_o);
		vEt = (EditText) findViewById(R.id.input_et_v);
		uEt = (EditText) findViewById(R.id.input_et_u);
		sEt = (EditText) findViewById(R.id.input_et_s);
		
		
		// autocomplete function
		adapter = new PatientAutocompleteAdapter(
				getApplicationContext(), 
				R.layout.autocomplete_patient, 
				null,
				modelController
			);
		nameEt.setAdapter(adapter);
		nameEt.setThreshold(2);
		nameEt.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), ""+id, Toast.LENGTH_LONG).show();
				Patient patient = adapter.getItem(position);
				roomEt.setText(patient.getRoom());
				typeEt.setText(patient.getType());
				hnEt.setText(patient.getHn());
				String birthdayStr = dateFormat.format(patient.getBirthdayDate());
				String birthdayStrThai = DateHelper.toThaiStr(birthdayStr);
				birthdayEt.setText(birthdayStrThai);
			}
			
		});
	}
	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		modelController.close();
		
		super.onDestroy();
	}


	@SuppressLint("SimpleDateFormat")
	private boolean validateInput() {
		boolean isValidated = true;
		boolean isDateValid = true;
		
		String name = nameEt.getText().toString();
		String room = roomEt.getText().toString();
		String type = typeEt.getText().toString();
		String birthdayStrThai = birthdayEt.getText().toString();
		//Log.e("birthday", birthdayStr);
		try {
			birthdayStr = DateHelper.fromThaiStr(birthdayStrThai);
			dateFormat.parse(birthdayStr);
			//Log.e("Birthday check", birthdayStr);
		} catch (NumberFormatException e) {
			isDateValid = false;
			e.printStackTrace();
		} catch (ParseException e) {
			isDateValid = false;
			e.printStackTrace();
		}
		
		if (name.trim().isEmpty() || room.trim().isEmpty() || type.trim().isEmpty()) {
			isValidated = false;
			Log.e("Valid not pass", "general info");
		}
		else if (!isDateValid) {
			isValidated = false;
			Log.e("Valid not pass", "birthday");
		}
		else {
			String p = pEt.getText().toString();
			String r = rEt.getText().toString();
			String bp = bpEt.getText().toString();
			String spo2 = spo2Et.getText().toString();
			
			if (p.trim().isEmpty() || r.trim().isEmpty() || bp.trim().isEmpty() || spo2.trim().isEmpty()) {
				isValidated = false;
				Log.e("Valid not pass", "score factor");
			}
			else if (!bp.matches("(\\d+)/(\\d+)")) {
				isValidated = false;
				Log.e("Valid not pass", "BP format");
			}
		}
		
		return isValidated;
	}
	
	
	private class SaveDataTask extends AsyncTask<Void, Void, Integer> {
		ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			int currentOrientation = getResources().getConfiguration().orientation;
			if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
			}
			else {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
			}
			
			dialog = new ProgressDialog(InputActivity.this);
			dialog.setMessage("กำลังบันทึกข้อมูล...");
			dialog.setCancelable(false);
			dialog.show();
			
			super.onPreExecute();
		}

		@Override
		protected Integer doInBackground(Void... params) {
			// TODO Auto-generated method stub
			Patient patient = new Patient(name, birthdayStr, room, type, hn);
			Patient existPatient = modelController.getPatient(patient.getName(), patient.getBirthday());
			if (existPatient == null) {
				patient = modelController.createPatient(patient);
			}
			else if (existPatient.getName().equals(patient.getName()) && existPatient.getBirthday().equals(patient.getBirthday())) {
				patient = existPatient;
				patient.setUpdatedAt(new GregorianCalendar());
				modelController.updatePatient(patient);
			}
			else {
				patient = modelController.createPatient(patient);
			}
			//Log.e("created at", new SimpleDateFormat(Patient.DATE_FORMAT).format(patient.getCreatedAt()));
			
			patientData = new PatientData(patient, p, r, bp, bpLower, spo2);
			patientData.setOtherData(t, o, v, u, s);
			patientData = modelController.createPatientData(patientData);
			
			return 1;
		}

		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			
			// go to score activity
			Intent scoreAct = new Intent(InputActivity.this, ScoreActivity.class);
			
			// patient data
			scoreAct.putExtra("p", p);
			scoreAct.putExtra("r", r);
			scoreAct.putExtra("bp", "" + bp + "/" + bpLower);
			scoreAct.putExtra("spo2", spo2);
			
			// patient data color
			scoreAct.putExtra("pState", patientData.getpState());
			scoreAct.putExtra("rState", patientData.getrState());
			scoreAct.putExtra("bpState", patientData.getBpState());
			scoreAct.putExtra("spo2State", patientData.getSpo2State());
			scoreAct.putExtra("resultState", patientData.getResultState());
			
			// result
			scoreAct.putExtra("pScore", patientData.getpScore());
			scoreAct.putExtra("rScore", patientData.getrScore());
			scoreAct.putExtra("bpScore", patientData.getBpScore());
			scoreAct.putExtra("spo2Score", patientData.getSpo2Score());
			scoreAct.putExtra("score", patientData.getScore());
			scoreAct.putExtra("resultStr", patientData.getResultStr());
			
			// content of other patient data
			StringBuilder otherSb = new StringBuilder();
			otherSb.append("ชื่อ-นามสกุล : " + name + "        ");
			otherSb.append("ห้อง : " + room + "\n");
			otherSb.append("ประเภทผู้ป่วย : " + type + "        ");
			otherSb.append("HN : " + hn + "        ");
			otherSb.append("วันเกิด : " + birthdayStrThai + "\n\n");
			otherSb.append(getResources().getString(R.string.var_t_full) + " : " + patientData.getTStr() + "        ");
			otherSb.append(getResources().getString(R.string.var_o_full) + " : " + patientData.getOStr() + "        ");
			otherSb.append(getResources().getString(R.string.var_v_full) + " : " + patientData.getVStr() + "        ");
			otherSb.append(getResources().getString(R.string.var_u_full) + " : " + patientData.getUStr() + "        ");
			otherSb.append(getResources().getString(R.string.var_s_full) + " : " + patientData.getSStr());
			
			scoreAct.putExtra("other", otherSb.toString());
			
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			
			startActivity(scoreAct);
			
			super.onPostExecute(result);
		}
		
	}
}
