package com.kiwilauncher.vitalsignalertsystem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import static android.widget.CompoundButton.*;


public class PatientsActivity extends Activity {
    private ImageButton headerBackBt;
    private EditText nameEt, dateEt;
    private Button searchBt, resetBt, deleteBt;
    private TableLayout table;

    private SimpleDateFormat dateFormat, timeFormat;
    private WorkingTimeRange timerange;

    private ModelController modelController;

    private BuildDataTableTask buildTableTask;
    private DeletePatientsTask deletePatientsTask;

    private List<Object> checkedPatientsId;

    private final String TIME_FORMAT = "HH:mm";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients);


        // --------------------------Set Header functions--------------------------
        TextView headerT = (TextView) findViewById(R.id.header_t_title);
        headerT.setText("จัดการข้อมูลผู้ป่วย");

        headerBackBt = (ImageButton) findViewById(R.id.header_imgbt_back);
        headerBackBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });


        // --------------------------Set Models & Controllers--------------------------
        modelController = new ModelController(getApplicationContext());
        modelController.open();

        dateFormat = new SimpleDateFormat(Patient.DATE_FORMAT);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
        timeFormat = new SimpleDateFormat(TIME_FORMAT);
        timeFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));

        timerange = new WorkingTimeRange(0);

        checkedPatientsId = new ArrayList<Object>();


        // --------------------------Set Views--------------------------
        nameEt = (EditText) findViewById(R.id.patients_et_name);

        dateEt = (EditText) findViewById(R.id.patients_et_date);

        searchBt = (Button) findViewById(R.id.patients_bt_search);
        searchBt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(PatientsActivity.this, v);

                if (buildTableTask.getStatus() == AsyncTask.Status.RUNNING) {
                    buildTableTask.cancel(true);
                }
                buildTableTask = new BuildDataTableTask();
                buildTableTask.execute();
            }
        });

        resetBt = (Button) findViewById(R.id.patients_bt_reset_search);
        resetBt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(PatientsActivity.this, v);

                nameEt.setText("");
                dateEt.setText("");

                if (buildTableTask.getStatus() == AsyncTask.Status.RUNNING) {
                    buildTableTask.cancel(true);
                }
                buildTableTask = new BuildDataTableTask();
                buildTableTask.execute();
            }
        });

        deleteBt = (Button) findViewById(R.id.patients_bt_delete);
        deleteBt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePatientsTask = new DeletePatientsTask();
                deletePatientsTask.execute();
            }
        });

        table = (TableLayout) findViewById(R.id.patients_table);


        // --------------------------Set Data from AsyncTask--------------------------
        buildTableTask = new BuildDataTableTask();
        buildTableTask.execute();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        modelController.close();

        if (buildTableTask.getStatus() == AsyncTask.Status.RUNNING) {
            buildTableTask.cancel(true);
        }

        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            buildTableTask = new BuildDataTableTask();
            buildTableTask.execute();
        }
    }


    private void hideSoftKeyboard (Activity activity, View view)
    {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }


    private class BuildDataTableTask extends AsyncTask<Void, Void, Integer> {
        private List<Patient> patientsList;

        private final int VALID = 1, INVALID_DATE = 2;

        @Override
        protected Integer doInBackground(Void... params) {
            // TODO Auto-generated method stub
            String nameStr = nameEt.getText().toString();
            String dateStrThai = dateEt.getText().toString();


            // --------------------------Blank inputs case--------------------------
            if (nameStr.equals("") && dateStrThai.equals("")) {
                patientsList = modelController.getRecentPatients();
            }

            // --------------------------Have inputs case--------------------------
            else if (dateStrThai.equals("")) {
                patientsList = modelController.getRecentPatients(nameStr);
            }
            else {
                // Validate date before build table
                boolean isDateValid = true;

                String dateStr;
                try {
                    dateStr = DateHelper.fromThaiStr(dateStrThai);
                    dateFormat.parse(dateStr);
                    //Log.e("Birthday check", birthdayStr);
                } catch (NumberFormatException e) {
                    isDateValid = false;
                    e.printStackTrace();
                } catch (ParseException e) {
                    isDateValid = false;
                    e.printStackTrace();
                }

                if (!isDateValid) return INVALID_DATE;

                dateStr = DateHelper.fromThaiStr(dateStrThai);
                String[] dateParts = dateStr.split("/");
                int day = Integer.parseInt(dateParts[0]);
                int month = Integer.parseInt(dateParts[1]);
                int year = Integer.parseInt(dateParts[2]);
                Calendar calendar = new GregorianCalendar(year, month-1, day);

                patientsList = modelController.getRecentPatients(nameStr, calendar, 0, 0, 24, 0);
            }

            if (isCancelled()) return 0;

            return VALID;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // TODO Auto-generated method stub
            switch (result) {
                case VALID:
                    buildDataTable();
                    break;

                case INVALID_DATE:
                    buildErrorDialog();
                    break;
            }

            super.onPostExecute(result);
        }

        @SuppressLint("InflateParams")
        private void buildDataTable() {
            table.removeViews(1, table.getChildCount() -1);

            if (!patientsList.isEmpty()) {
                int count = 0;
                for (Patient pt : patientsList) {
                    count++;

                    PatientData recentPd = null;
                    if (pt.getPatientDataList() != null) {
                        recentPd = pt.getPatientDataList().get(0);
                    }


                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                    TableRow tr = (TableRow) inflater.inflate(R.layout.tablerow_patients, null);

                    // Set data
                    CheckBox cb = (CheckBox) tr.findViewWithTag("patients_cb_template");
                    cb.setTag(pt.getId());
                    cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                checkedPatientsId.add(buttonView.getTag());
                            } else {
                                int index = checkedPatientsId.indexOf(buttonView.getTag());
                                checkedPatientsId.remove(index);
                            }
                        }
                    });

                    TextView roomT = (TextView) tr.findViewById(R.id.patients_td_room);
                    roomT.setText(pt.getRoom());

                    TextView datetimeT = (TextView) tr.findViewById(R.id.patients_td_datetime);
                    datetimeT.setText(DateHelper.toThaiStr(dateFormat.format(pt.getUpdatedAtDate())) + " " + timeFormat.format(pt.getUpdatedAtDate()));

                    TextView nameT = (TextView) tr.findViewById(R.id.patients_td_name);
                    nameT.setPaintFlags(nameT.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    nameT.setText(pt.getName());
                    nameT.setTag(pt);
                    nameT.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Patient patient = (Patient) v.getTag();
                            Intent patientDataAct = new Intent(getApplicationContext(), PatientDataActivity.class);
                            patientDataAct.putExtra("patient_id", patient.getId());
                            startActivityForResult(patientDataAct, 1);
                        }
                    });

                    TextView scoreT = (TextView) tr.findViewById(R.id.patients_td_score);
                    if (recentPd != null) {
                        scoreT.setText("" + recentPd.getScore());

                        // Set color
                        if (recentPd.getScore() > 2) {
                            scoreT.setTextColor(getStateColor(recentPd.getResultState()));
                        }
                    }
                    else {
                        scoreT.setText("-");
                    }

                    if (count%2 == 0) {
                        tr.setBackgroundColor(getResources().getColor(R.color.almost_white));
                    }

                    table.addView(tr);
                }
            }
        }

        private void buildErrorDialog() {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(PatientsActivity.this);
            alertBuilder.setMessage("โปรดใส่วันที่ให้ถูกต้อง");
            alertBuilder.setNeutralButton("OK", null);
            AlertDialog alert = alertBuilder.create();
            alert.show();
        }
    }


    private class DeletePatientsTask extends AsyncTask<Void, Void, Integer> {
        private List<Patient> patientsList;

        private final int VALID = 1, INVALID_LIST = 2;

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            int currentOrientation = getResources().getConfiguration().orientation;
            if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            }
            else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }

            dialog = new ProgressDialog(PatientsActivity.this);
            dialog.setMessage("กำลังลบข้อมูล...");
            dialog.setCancelable(false);
            dialog.show();

            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            if (checkedPatientsId.size() == 0) {
                return INVALID_LIST;
            }

            modelController.deletePatients(checkedPatientsId);

            return VALID;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // TODO Auto-generated method stub
            dialog.dismiss();

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

            switch (result) {
                case VALID:
                    buildDataTable();
                    break;

                case INVALID_LIST:
                    buildErrorDialog();
                    break;
            }

            super.onPostExecute(result);
        }

        private void buildDataTable() {
            if (buildTableTask.getStatus() == AsyncTask.Status.RUNNING) {
                buildTableTask.cancel(true);
            }
            buildTableTask = new BuildDataTableTask();
            buildTableTask.execute();
        }

        private void buildErrorDialog() {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(PatientsActivity.this);
            alertBuilder.setMessage("โปรดเลือกข้อมูลที่ต้องการลบ");
            alertBuilder.setNeutralButton("OK", null);
            AlertDialog alert = alertBuilder.create();
            alert.show();
        }
    }


    private int getStateColor(int state) {
        int color;
        switch (state) {
            case PatientData.NORMAL_STATE:
                color = getResources().getColor(R.color.normal_state);
                break;
            case PatientData.WARNING_STATE:
                color = getResources().getColor(R.color.warning_state);
                break;
            case PatientData.ALERT_STATE:
                color = getResources().getColor(R.color.alert_state);
                break;
            default:
                color = getResources().getColor(R.color.black);
                break;
        }

        return color;
    }
}
