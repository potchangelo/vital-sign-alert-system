package com.kiwilauncher.vitalsignalertsystem;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

import android.provider.BaseColumns;


public class PatientData {
	private long id;
	
	private int p, r, bp, bpLower, spo2;
	
	private int pScore, rScore, bpScore, spo2Score, score;
	private int pState, rState, bpState, spo2State, resultState;
	
	private String t;
	private String o, v, u, s;
	
	private Calendar createdAt;
	private Calendar updatedAt;
	
	private Patient patient;
	
	private String resultStr;
	
	public static abstract class PatientDataEntry implements BaseColumns {
		public static final String TABLE_NAME = "patient_data";
		public static final String COLUMN_PATIENT_ID = "patient_id", 
				COLUMN_P = "p", COLUMN_R = "r", COLUMN_BP = "bp", COLUMN_BP_LOWER = "bp_lower", COLUMN_SPO2 = "spo2",
				COLUMN_T = "t", COLUMN_O = "o", COLUMN_V = "v", COLUMN_U = "u", COLUMN_S = "s", 
				COLUMN_CREATED_AT = "created_at", COLUMN_UPDATED_AT = "updated_at";
		
	}
	
	public static final int NORMAL_STATE = 0, WARNING_STATE = 1, ALERT_STATE = 2;
	
	public static Comparator<PatientData> ROOM_SORT = new Comparator<PatientData>() {

		@Override
		public int compare(PatientData lhs, PatientData rhs) {
			// TODO Auto-generated method stub
			String room1 = lhs.getPatient().getRoom();
			String room2 = rhs.getPatient().getRoom();
			
			return room1.compareTo(room2);
		}
	};
	
	
	public PatientData(Patient patient, int p, int r, int bp, int bpLower, int spo2) {
		super();
		this.patient = patient;
		this.p = p;
		this.r = r;
		this.bp = bp;
		this.bpLower = bpLower;
		this.spo2 = spo2;
		createdAt = new GregorianCalendar();
		updatedAt = new GregorianCalendar();
	}
	
	public PatientData(Patient patient, long id, int p, int r, int bp, int bpLower, int spo2) {
		super();
		this.patient = patient;
		this.id = id;
		this.p = p;
		this.r = r;
		this.bp = bp;
		this.bpLower = bpLower;
		this.spo2 = spo2;
		createdAt = new GregorianCalendar();
		updatedAt = new GregorianCalendar();
	}

	public void setOtherData(String t, String o, String v, String u, String s) {
		this.t = t;
		this.o = o;
		this.v = v;
		this.u = u;
		this.s = s;
	}

	public void calculateScore() {
		// total score
		score = 0;
		
		// P
		if (p>=60 && p<=100) {
			pScore = 0;
			pState = NORMAL_STATE;
		}
		else if ((p>=50 && p<=59) || (p>=101 && p<=119)) {
			pScore = 1;
			score+=1;
			pState = WARNING_STATE;
		}
		else if (p<=49 || p>=120) {
			pScore = 2;
			score+=2;
			pState = ALERT_STATE;
		}
		
		// R
		if (r>=16 && r<=20) {
			rScore = 0;
			rState = NORMAL_STATE;
		}
		else if ((r>=11 && r<=15) || (r>=21 && r<=29)) {
			rScore = 1;
			score+=1;
			rState = WARNING_STATE;
		}
		else if (r<=10 || r>=30) {
			rScore = 2;
			score+=2;
			rState = ALERT_STATE;
		}
		
		// BP
		if (bp>=100 && bp<=140) {
			bpScore = 0;
			bpState = NORMAL_STATE;
		}
		else if ((bp>=90 && bp<=99) || (bp>=141 && bp<=180)) {
			bpScore = 1;
			score+=1;
			bpState = WARNING_STATE;
		}
		else if (bp<=89 || bp>=181) {
			bpScore = 2;
			score+=2;
			bpState = ALERT_STATE;
		}
		
		// SpO2
		if (spo2>=95) {
			spo2Score = 0;
			spo2State = NORMAL_STATE;
		}
		if (spo2>=90 && spo2<=94) {
			spo2Score = 1;
			score+=1;
			spo2State = WARNING_STATE;
		}
		else if (spo2<=89) {
			spo2Score = 2;
			score+=2;
			spo2State = ALERT_STATE;
		}
		
		// result
		if (score <=2) {
			resultStr = "อยู่ในระดับปกติ";
			resultState = NORMAL_STATE;
		}
		else if (score >= 3 && score <=4) {
			resultStr = "อยู่ในระดับประเมินผู้ป่วย";
			resultState = WARNING_STATE;
		}
		else if (score >=5 && score <=8) {
			resultStr = "อยู่ในระดับเร่งให้การรักษา";
			resultState = ALERT_STATE;
		}
	}

	public long getId() {
		return id;
	}
	
	public Patient getPatient() {
		return patient;
	}

	public int getP() {
		return p;
	}

	public void setP(int p) {
		this.p = p;
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}

	public int getBp() {
		return bp;
	}

	public void setBp(int bp) {
		this.bp = bp;
	}

	public int getBpLower() {
		return bpLower;
	}

	public void setBpLower(int bpLower) {
		this.bpLower = bpLower;
	}

	public int getSpo2() {
		return spo2;
	}

	public void setSpo2(int spo2) {
		this.spo2 = spo2;
	}

	public int getpScore() {
		return pScore;
	}

	public int getrScore() {
		return rScore;
	}

	public int getBpScore() {
		return bpScore;
	}

	public int getSpo2Score() {
		return spo2Score;
	}

	public int getScore() {
		return score;
	}

	public int getpState() {
		return pState;
	}

	public int getrState() {
		return rState;
	}

	public int getBpState() {
		return bpState;
	}

	public int getSpo2State() {
		return spo2State;
	}

	public int getResultState() {
		return resultState;
	}

	public String getResultStr() {
		return resultStr;
	}


	public String getT() {
		return t;
	}
	
	public String getTStr() {
		if (t == null) {
			return "-";
		}
		return ""+t;
	}

	public void setT(String t) {
		this.t = t;
	}

	public String getO() {
		return o;
	}
	
	public String getOStr() {
		if (o == null) {
			return "-";
		}
		return ""+o;
	}

	public void setO(String o) {
		this.o = o;
	}

	public String getV() {
		return v;
	}
	
	public String getVStr() {
		if (v == null) {
			return "-";
		}
		return ""+v;
	}

	public void setV(String v) {
		this.v = v;
	}

	public String getU() {
		return u;
	}
	
	public String getUStr() {
		if (u == null) {
			return "-";
		}
		return ""+u;
	}

	public void setU(String u) {
		this.u = u;
	}

	public String getS() {
		return s;
	}
	
	public String getSStr() {
		if (s == null) {
			return "-";
		}
		return ""+s;
	}

	public void setS(String s) {
		this.s = s;
	}

	public Calendar getCreatedAt() {
		return createdAt;
	}
	
	public Date getCreatedAtDate() {
		return createdAt.getTime();
	}

	public void setCreatedAt(Calendar createdAt) {
		this.createdAt = createdAt;
	}

	public Calendar getUpdatedAt() {
		return updatedAt;
	}
	
	public Date getUpdatedAtDate() {
		return updatedAt.getTime();
	}

	public void setUpdatedAt(Calendar updatedAt) {
		this.updatedAt = updatedAt;
	}

}
