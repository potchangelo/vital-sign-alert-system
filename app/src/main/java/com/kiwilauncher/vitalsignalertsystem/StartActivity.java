package com.kiwilauncher.vitalsignalertsystem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class StartActivity extends Activity {
	Button startBt, filldataBt, resultBt, patientsBt, aboutBt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		
		startBt = (Button) findViewById(R.id.start_bt_start);
		startBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent inputAct = new Intent(getApplicationContext(), InputActivity.class);
				startActivity(inputAct);
			}
		});
		
		filldataBt = (Button) findViewById(R.id.start_bt_filldata);
		filldataBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent filldataAct = new Intent(getApplicationContext(), FilldataActivity.class);
				startActivity(filldataAct);
			}
		});
		
		resultBt = (Button) findViewById(R.id.start_bt_result);
		resultBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent resultAct = new Intent(getApplicationContext(), ResultActivity.class);
				startActivity(resultAct);
			}
		});

		patientsBt = (Button) findViewById(R.id.start_bt_patients);
		patientsBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent patientsAct = new Intent(getApplicationContext(), PatientsActivity.class);
				startActivity(patientsAct);
			}
		});
		
		aboutBt = (Button) findViewById(R.id.start_bt_about);
		aboutBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent aboutAct = new Intent(getApplicationContext(), AboutActivity.class);
				startActivity(aboutAct);
			}
		});
	}
}
