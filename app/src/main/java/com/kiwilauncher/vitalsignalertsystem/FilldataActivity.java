package com.kiwilauncher.vitalsignalertsystem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class FilldataActivity extends Activity {
	Button saveBt;
	ImageButton headerBackBt;
	AutoCompleteTextView nameEt;
	EditText roomEt, typeEt, hnEt, birthdayEt;
	TextView savedinfoT;
	
	PatientAutocompleteAdapter adapter;
	
	ModelController modelController;
	Patient patient;
	SimpleDateFormat dateFormat;
	
	String name, room, type, hn, birthdayStr, birthdayStrThai;

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_filldata);
		
		modelController = new ModelController(getApplicationContext());
		modelController.open();
		
		dateFormat = new SimpleDateFormat(Patient.DATE_FORMAT);
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
		
		TextView headerT = (TextView) findViewById(R.id.header_t_title);
		headerT.setText("ใส่ข้อมูลผู้ป่วย");
		
		saveBt = (Button) findViewById(R.id.input_bt_save);
		saveBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// validate inputs
				if (validateInput()) {
					saveBt.setEnabled(false);
					saveBt.setText("กำลังบันทึกข้อมูล...");
					saveBt.setTextColor(getResources().getColor(R.color.halfup_gray));
					
					// get inputs
					name = nameEt.getText().toString();
					room = roomEt.getText().toString();
					type = typeEt.getText().toString();
					hn = hnEt.getText().toString();
					birthdayStrThai = birthdayEt.getText().toString();
					String yearStr = birthdayStrThai.substring(birthdayStrThai.lastIndexOf("/") + 1);
					int year = Integer.parseInt(yearStr) - 543;
					birthdayStr = birthdayStrThai.replace(yearStr, "" + year);
					
					new SaveDataTask().execute();
				}
				else {
					// display error
					AlertDialog.Builder alertBuilder = new AlertDialog.Builder(FilldataActivity.this);
					alertBuilder.setMessage("โปรดใส่ข้อมูลให้ถูกต้องและครบถ้วน");
					alertBuilder.setNeutralButton("OK", null);
					AlertDialog alert = alertBuilder.create();
					alert.show();
				}
			}
		});
		
		headerBackBt = (ImageButton) findViewById(R.id.header_imgbt_back);
		headerBackBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		nameEt = (AutoCompleteTextView) findViewById(R.id.input_et_name);
		roomEt = (EditText) findViewById(R.id.input_et_room);
		typeEt = (EditText) findViewById(R.id.input_et_type);
		hnEt = (EditText) findViewById(R.id.input_et_hn);
		birthdayEt = (EditText) findViewById(R.id.input_et_birthday);
		
		savedinfoT = (TextView) findViewById(R.id.filldata_t_savedinfo);
		
		// autocomplete function
		adapter = new PatientAutocompleteAdapter(
				getApplicationContext(), 
				R.layout.autocomplete_patient, 
				null,
				modelController
			);
		nameEt.setAdapter(adapter);
		nameEt.setThreshold(2);
		nameEt.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), ""+id, Toast.LENGTH_LONG).show();
				Patient patientSelected = adapter.getItem(position);
				roomEt.setText(patientSelected.getRoom());
				typeEt.setText(patientSelected.getType());
				hnEt.setText(patientSelected.getHn());
				String birthdayStr = dateFormat.format(patientSelected.getBirthdayDate());
				String yearStr = birthdayStr.substring(birthdayStr.lastIndexOf("/") + 1);
				int year = Integer.parseInt(yearStr) + 543;
				String birthdayStrThai = birthdayStr.replace(yearStr, "" + year);
				birthdayEt.setText(birthdayStrThai);
			}
			
		});
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		modelController.close();
		
		super.onDestroy();
	}
	
	@SuppressLint("SimpleDateFormat")
	private boolean validateInput() {
		boolean isValidated = true;
		boolean isDateValid = true;
		int year = 0;
		
		String name = nameEt.getText().toString();
		String room = roomEt.getText().toString();
		String type = typeEt.getText().toString();
		String birthdatStrThai = birthdayEt.getText().toString();
		//Log.e("birthday", birthdayStr);
		try {
			String yearStr = birthdatStrThai.substring(birthdatStrThai.lastIndexOf("/") + 1);
			year = Integer.parseInt(yearStr) - 543;
			birthdayStr = birthdatStrThai.replace(yearStr, "" + year);
			dateFormat.parse(birthdayStr);
		} catch (NumberFormatException e) {
			isDateValid = false;
			e.printStackTrace();
		} catch (ParseException e) {
			isDateValid = false;
			e.printStackTrace();
		}
		
		if (name.trim().isEmpty() || room.trim().isEmpty() || type.trim().isEmpty()) {
			isValidated = false;
			Log.e("Valid not pass", "general info");
		}
		else if (!isDateValid) {
			isValidated = false;
			Log.e("Valid not pass", "birthday");
		}
		
		return isValidated;
	}
	
	private class SaveDataTask extends AsyncTask<Void, Void, Integer> {
		ProgressDialog dialog;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			int currentOrientation = getResources().getConfiguration().orientation;
			if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
			}
			else {
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
			}
			
			dialog = new ProgressDialog(FilldataActivity.this);
			dialog.setMessage("กำลังบันทึกข้อมูล...");
			dialog.setCancelable(false);
			dialog.show();
			
			super.onPreExecute();
		}

		@Override
		protected Integer doInBackground(Void... params) {
			// TODO Auto-generated method stub
			patient = new Patient(name, birthdayStr, room, type, hn);
			Patient existPatient = modelController.getPatient(patient.getName(), patient.getBirthday());
			if (existPatient == null) {
				patient = modelController.createPatient(patient);
			}
			else if (existPatient.getName().equals(patient.getName()) && existPatient.getBirthday().equals(patient.getBirthday())) {
				patient.setId(existPatient.getId());
				patient.setUpdatedAt(new GregorianCalendar());
				modelController.updatePatient(patient);

			}
			else {
				patient = modelController.createPatient(patient);
			}
			
			return 1;
		}

		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			dialog.dismiss();
			
			StringBuilder sb = new StringBuilder();
			sb.append("ชื่อ-นามสกุล : " + patient.getName() + "\n");
			sb.append("ห้อง : " + patient.getRoom() + "\n");
			sb.append("ประเภทผู้ป่วย : " + patient.getType() + "\n");
			sb.append("HN : " + patient.getHn() + "\n");
			sb.append("วันเกิด : " + birthdayStrThai + "\n");
			
			savedinfoT.setText(sb.toString());
			
			// enable UI
			nameEt.setText("");
			roomEt.setText(""); 
			typeEt.setText("");
			hnEt.setText("");
			birthdayEt.setText("");
			
			saveBt.setEnabled(true);
			saveBt.setText("บันทึกข้อมูล");
			saveBt.setTextColor(getResources().getColor(R.color.white));
			
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			
			super.onPostExecute(result);
		}
		
	}
}
