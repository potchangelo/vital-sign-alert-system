package com.kiwilauncher.vitalsignalertsystem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class ResultActivity extends Activity {
	ImageButton headerBackBt;
	private EditText dateEt;
	private Button refreshBt, printBt;
	private Spinner timeSpn;
	private TableLayout table;
	
	private SimpleDateFormat dateFormat, timeFormat;
	
	private ModelController modelController;
	private Calendar calendar;
	private WorkingTimeRange timerange;
	
	private BuildDataTableTask buildTableTask;
	private PrintDataTableTask printTableTask;
	
	private final String TIME_FORMAT = "HH:mm";

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);
		
		TextView headerT = (TextView) findViewById(R.id.header_t_title);
		headerT.setText("สรุปข้อมูลผู้ป่วย");
		
		modelController = new ModelController(getApplicationContext());
		modelController.open();
		
		table = (TableLayout) findViewById(R.id.result_table);
		
		// Set current date and time range
		dateFormat = new SimpleDateFormat(Patient.DATE_FORMAT);
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
		timeFormat = new SimpleDateFormat(TIME_FORMAT);
		timeFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
		
		Calendar calendarNow = Calendar.getInstance();
		int year = calendarNow.get(Calendar.YEAR);
        int month = calendarNow.get(Calendar.MONTH);
        int day = calendarNow.get(Calendar.DAY_OF_MONTH);
		calendar = new GregorianCalendar(year, month, day);
		
		dateEt = (EditText) findViewById(R.id.result_et_date);
		String dateNow = dateFormat.format(calendar.getTime());
		String dateNowThai = DateHelper.toThaiStr(dateNow);
		dateEt.setText(dateNowThai);
		
		timerange = new WorkingTimeRange(0);
		timerange.setCurrentIndex(timerange.getIndex(calendarNow));
		timeSpn = (Spinner) findViewById(R.id.result_spn_time);
		timeSpn.setSelection(timerange.getIndex(calendarNow));
		timeSpn.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				timerange.setCurrentIndex(position);
				//buildDataTable();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				Log.e("time", "no select");
			}
		});
		
		refreshBt = (Button) findViewById(R.id.result_bt_refresh);
		refreshBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				hideSoftKeyboard(ResultActivity.this, v);

				if (buildTableTask.getStatus() == AsyncTask.Status.RUNNING) {
					buildTableTask.cancel(true);
				}
				buildTableTask = new BuildDataTableTask();
				buildTableTask.execute();
			}
		});
		
		printBt = (Button) findViewById(R.id.result_bt_print);
		printBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				printBt.setEnabled(false);
				printTableTask = new PrintDataTableTask();
				printTableTask.execute();
			}
		});
		
		// Set button
		headerBackBt = (ImageButton) findViewById(R.id.header_imgbt_back);
		headerBackBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		buildTableTask = new BuildDataTableTask();
		buildTableTask.execute();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		modelController.close();
		
		if (buildTableTask.getStatus() == AsyncTask.Status.RUNNING) {
			buildTableTask.cancel(true);
		}
		
		if (printTableTask != null) {
			if (printTableTask.getStatus() == AsyncTask.Status.RUNNING) {
				printTableTask.cancel(true);
			}
		}
		
		super.onDestroy();
	}


	private void hideSoftKeyboard (Activity activity, View view)
	{
		InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
	}
	
	private class BuildDataTableTask extends AsyncTask<Void, Void, Integer> {
		private List<PatientData> patientDataList;
		
		private final int VALID = 1, INVALID_DATE = 2;

		@Override
		protected Integer doInBackground(Void... params) {
			// TODO Auto-generated method stub
			// Validate date before build table
			boolean isDateValid = true;
			String dateStrThai = dateEt.getText().toString();
			String dateStr;
			try {
				dateStr = DateHelper.fromThaiStr(dateStrThai);
				dateFormat.parse(dateStr);
				//Log.e("Birthday check", birthdayStr);
			} catch (NumberFormatException e) {
				isDateValid = false;
				e.printStackTrace();
			} catch (ParseException e) {
				isDateValid = false;
				e.printStackTrace();
			}
			
			if (!isDateValid) return INVALID_DATE;
			
			dateStr = DateHelper.fromThaiStr(dateStrThai);
			String[] dateParts = dateStr.split("/");
			int day = Integer.parseInt(dateParts[0]);
			int month = Integer.parseInt(dateParts[1]);
			int year = Integer.parseInt(dateParts[2]);
			//Log.e("dmy", "d:" + day + " m:" + month + " y:" + year);
			calendar = new GregorianCalendar(year, month-1, day);
			//Log.e("calendar", "y:"+ calendar.get(Calendar.YEAR) + " m:" + calendar.get(Calendar.MONTH) + " d:" + calendar.get(Calendar.DAY_OF_MONTH));
			
			int lowerH = timerange.getSelectedLowerHour();
			int lowerM = timerange.getSelectedLowerMinute();
			int upperH = timerange.getSelectedUpperHour();
			int upperM = timerange.getSelectedUpperMinute();
			patientDataList = modelController.getPatientData(calendar, lowerH, lowerM, upperH, upperM);
			
			if (isCancelled()) return 0;
			
			return VALID;
		}

		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			switch (result) {
				case VALID: 
					buildDataTable();
					break;
					
				case INVALID_DATE:
					buildErrorDialog();
					break;
			}
			
			super.onPostExecute(result);
		}
		
		@SuppressLint("InflateParams")
		private void buildDataTable() {
			table.removeViews(1, table.getChildCount() -1);
			
			if (!patientDataList.isEmpty()) {
				int count = 0;
				for (PatientData pd : patientDataList) {
					count++;
					LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
					TableRow tr = (TableRow) inflater.inflate(R.layout.tablerow_result, null);
					
					// General info
					TextView roomT = (TextView) tr.findViewById(R.id.result_td_room);
					roomT.setText(pd.getPatient().getRoom());
					TextView timeT = (TextView) tr.findViewById(R.id.result_td_time);
					timeT.setText(timeFormat.format(pd.getCreatedAtDate()));
					TextView nameT = (TextView) tr.findViewById(R.id.result_td_name);
					nameT.setText(pd.getPatient().getName());
					
					// Score info
					TextView scoreT = (TextView) tr.findViewById(R.id.result_td_score);
					scoreT.setText("" + pd.getScore());
					TextView pT = (TextView) tr.findViewById(R.id.result_td_p);
					pT.setText("" + pd.getP());
					TextView rT = (TextView) tr.findViewById(R.id.result_td_r);
					rT.setText("" + pd.getR());
					TextView bpT = (TextView) tr.findViewById(R.id.result_td_bp);
					bpT.setText("" + pd.getBp() + "/" + pd.getBpLower());
					TextView spo2T = (TextView) tr.findViewById(R.id.result_td_spo2);
					spo2T.setText("" + pd.getSpo2());
					
					// Other info
					TextView tT = (TextView) tr.findViewById(R.id.result_td_t);
					tT.setText("" + pd.getTStr());
					TextView oT = (TextView) tr.findViewById(R.id.result_td_o);
					oT.setText("" + pd.getOStr());
					TextView vT = (TextView) tr.findViewById(R.id.result_td_v);
					vT.setText("" + pd.getVStr());
					TextView uT = (TextView) tr.findViewById(R.id.result_td_u);
					uT.setText("" + pd.getUStr());
					TextView sT = (TextView) tr.findViewById(R.id.result_td_s);
					sT.setText("" + pd.getSStr());
					
					// Set color
					if (pd.getScore() > 2) {
						scoreT.setTextColor(getStateColor(pd.getResultState()));
					}
					if (pd.getpScore() > 0) {
						pT.setTextColor(getStateColor(pd.getpState()));
					}
					if (pd.getrScore() > 0) {
						rT.setTextColor(getStateColor(pd.getrState()));
					}
					if (pd.getBpScore() > 0) {
						bpT.setTextColor(getStateColor(pd.getBpState()));
					}
					if (pd.getSpo2Score() > 0) {
						spo2T.setTextColor(getStateColor(pd.getSpo2State()));
					}
					
					if (count%2 == 0) {
						tr.setBackgroundColor(getResources().getColor(R.color.almost_white));
					}
					
					table.addView(tr);
				}
			}
		}
		
		private void buildErrorDialog() {
			AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ResultActivity.this);
			alertBuilder.setMessage("โปรดใส่วันที่ให้ถูกต้อง");
			alertBuilder.setNeutralButton("OK", null);
			AlertDialog alert = alertBuilder.create();
			alert.show();
		}
	}
	
	
	private class PrintDataTableTask extends AsyncTask<Void, Void, Integer> {
		private List<PatientData> patientDataList;
		private String dateStrThai;
		
		private final int VALID = 1, INVALID_DATE = 2;

		@Override
		protected Integer doInBackground(Void... params) {
			// TODO Auto-generated method stub
			boolean isDateValid = true;
			dateStrThai = dateEt.getText().toString();
			String dateStr;
			try {
				dateStr = DateHelper.fromThaiStr(dateStrThai);
				dateFormat.parse(dateStr);
				//Log.e("Birthday check", birthdayStr);
			} catch (NumberFormatException e) {
				isDateValid = false;
				e.printStackTrace();
			} catch (ParseException e) {
				isDateValid = false;
				e.printStackTrace();
			}
			
			if (!isDateValid) return INVALID_DATE;
			
			dateStr = DateHelper.fromThaiStr(dateStrThai);
			String[] dateParts = dateStr.split("/");
			int day = Integer.parseInt(dateParts[0]);
			int month = Integer.parseInt(dateParts[1]);
			int year = Integer.parseInt(dateParts[2]);
			//Log.e("dmy", "d:" + day + " m:" + month + " y:" + year);
			calendar = new GregorianCalendar(year, month-1, day);
			//Log.e("calendar", "y:"+ calendar.get(Calendar.YEAR) + " m:" + calendar.get(Calendar.MONTH) + " d:" + calendar.get(Calendar.DAY_OF_MONTH));
			
			int lowerH = timerange.getSelectedLowerHour();
			int lowerM = timerange.getSelectedLowerMinute();
			int upperH = timerange.getSelectedUpperHour();
			int upperM = timerange.getSelectedUpperMinute();
			patientDataList = modelController.getPatientData(calendar, lowerH, lowerM, upperH, upperM);
			//Log.e("build table", "data count = " + patientDataList.size());
			
			if (isCancelled()) return 0;
			
			return VALID;
		}

		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			switch (result) {
				case VALID: 
					buildPrintDocument();
					break;
					
				case INVALID_DATE:
					buildErrorDialog();
					break;
			}
			
			printBt.setEnabled(true);
			
			super.onPostExecute(result);
		}
		
		
		private void buildPrintDocument() {
			WebView webView = new WebView(getApplicationContext());
			webView.setWebViewClient(new WebViewClient() {

	            public boolean shouldOverrideUrlLoading(WebView view, String url) {
	                return false;
	            }

	            @Override
	            public void onPageFinished(WebView view, String url) {
	                Log.i("webview", "page finished loading " + url);
	                createWebPrintJob(view);
	            }
			});
			
			StringBuilder htmlBuilder = new StringBuilder();
			
			// Open Document
			htmlBuilder.append("<html>");
			htmlBuilder.append("<head>");
			htmlBuilder.append("<title>Patient report</title>");
			htmlBuilder.append("<style>");
			htmlBuilder.append("h1 { font-size: 18px; text-align:center; }");
			htmlBuilder.append("table { background-color:white; border-collapse:collapse; width:100%; }");
			htmlBuilder.append("table th, table td { border:1px solid #000; padding:5px; text-align:center }");
			htmlBuilder.append("table td.warning { color: #ffaa00; }");
			htmlBuilder.append("table td.alert { color: #ff2400; }");
			htmlBuilder.append("</style>");
			htmlBuilder.append("</head>");
			htmlBuilder.append("<body>");
			
			// Title
			htmlBuilder.append("<h1>รายงานผู้ป่วย วันที่ : " + dateStrThai + " เวลา : " + timeSpn.getSelectedItem().toString() + "</h1>");
			
			// Open Table
			htmlBuilder.append("<table>");
			
			// Table Header row
			htmlBuilder.append("<thead><tr>");
			htmlBuilder.append("<th style='width:20%;'>เตียง/ห้อง</th>");
			htmlBuilder.append("<th>เวลา</th>");
			htmlBuilder.append("<th style='width:30%;'>ชื่อ-นามสกุล</th>");
			htmlBuilder.append("<th>Score</th>");
			htmlBuilder.append("<th>P</th>");
			htmlBuilder.append("<th>R</th>");
			htmlBuilder.append("<th>BP</th>");
			htmlBuilder.append("<th>SpO2</th>");
			htmlBuilder.append("<th>T</th>");
			htmlBuilder.append("<th>O</th>");
			htmlBuilder.append("<th>V</th>");
			htmlBuilder.append("<th>U</th>");
			htmlBuilder.append("<th>S</th>");
			htmlBuilder.append("</tr></thead>");
			
			// Table Data row
			htmlBuilder.append("<tbody>");
			
			if (!patientDataList.isEmpty()) {
				for (PatientData pd : patientDataList) {
					htmlBuilder.append("<tr>");
					
					// General info
					htmlBuilder.append("<td>" + pd.getPatient().getRoom() + "</td>");
					htmlBuilder.append("<td>" + timeFormat.format(pd.getCreatedAtDate()) + "</td>");
					htmlBuilder.append("<td>" + pd.getPatient().getName() + "</td>");
					
					// Score info
					// Score
					if (pd.getResultState() == PatientData.ALERT_STATE) {
						htmlBuilder.append("<td class='alert'>" + pd.getScore() + "</td>");
					}
					else if (pd.getResultState() == PatientData.WARNING_STATE) {
						htmlBuilder.append("<td class='warning'>" + pd.getScore() + "</td>");
					}
					else {
						htmlBuilder.append("<td>" + pd.getScore() + "</td>");
					}
					
					// P
					if (pd.getpState() == PatientData.ALERT_STATE) {
						htmlBuilder.append("<td class='alert'>" + pd.getP() + "</td>");
					}
					else if (pd.getpState() == PatientData.WARNING_STATE) {
						htmlBuilder.append("<td class='warning'>" + pd.getP() + "</td>");
					}
					else {
						htmlBuilder.append("<td>" + pd.getP() + "</td>");
					}
					
					// R
					if (pd.getrState() == PatientData.ALERT_STATE) {
						htmlBuilder.append("<td class='alert'>" + pd.getR() + "</td>");
					}
					else if (pd.getrState() == PatientData.WARNING_STATE) {
						htmlBuilder.append("<td class='warning'>" + pd.getR() + "</td>");
					}
					else {
						htmlBuilder.append("<td>" + pd.getR() + "</td>");
					}
					
					// BP
					if (pd.getBpState() == PatientData.ALERT_STATE) {
						htmlBuilder.append("<td class='alert'>" + pd.getBp() + "/" + pd.getBpLower() + "</td>");
					}
					else if (pd.getBpState() == PatientData.WARNING_STATE) {
						htmlBuilder.append("<td class='warning'>" + pd.getBp() + "/" + pd.getBpLower() + "</td>");
					}
					else {
						htmlBuilder.append("<td>" + pd.getBp() + "/" + pd.getBpLower() + "</td>");
					}
					
					// SpO2
					if (pd.getSpo2State() == PatientData.ALERT_STATE) {
						htmlBuilder.append("<td class='alert'>" + pd.getSpo2() + "</td>");
					}
					else if (pd.getSpo2State() == PatientData.WARNING_STATE) {
						htmlBuilder.append("<td class='warning'>" + pd.getSpo2() + "</td>");
					}
					else {
						htmlBuilder.append("<td>" + pd.getSpo2() + "</td>");
					}
					
					// Other info
					htmlBuilder.append("<td>" + pd.getTStr() + "</td>");
					htmlBuilder.append("<td>" + pd.getOStr() + "</td>");
					htmlBuilder.append("<td>" + pd.getVStr() + "</td>");
					htmlBuilder.append("<td>" + pd.getUStr() + "</td>");
					htmlBuilder.append("<td>" + pd.getSStr() + "</td>");
					
					htmlBuilder.append("</tr>");
				}
			}
			
			htmlBuilder.append("</tbody>");
			
			// Close Table
			htmlBuilder.append("</table>");
			
			// Close Document
			htmlBuilder.append("</body></html>");
			webView.loadDataWithBaseURL(null, htmlBuilder.toString(), "text/HTML", "UTF-8", null);
		}
		
		
		private void buildErrorDialog() {
			AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ResultActivity.this);
			alertBuilder.setMessage("โปรดใส่วันที่ให้ถูกต้อง");
			alertBuilder.setNeutralButton("OK", null);
			AlertDialog alert = alertBuilder.create();
			alert.show();
		}
	}
	
	
	private int getStateColor(int state) {
		int color;
		switch (state) {
			case PatientData.NORMAL_STATE:
				color = getResources().getColor(R.color.normal_state);
				break;
			case PatientData.WARNING_STATE:
				color = getResources().getColor(R.color.warning_state);
				break;
			case PatientData.ALERT_STATE:
				color = getResources().getColor(R.color.alert_state);
				break;
			default:
				color = getResources().getColor(R.color.black);
				break;
		}
		
		return color;
	}
	
	
	/*public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		private boolean isFired;

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			Calendar calendarNow = Calendar.getInstance();
			int year = calendarNow.get(Calendar.YEAR);
	        int month = calendarNow.get(Calendar.MONTH);
	        int day = calendarNow.get(Calendar.DAY_OF_MONTH);
	        
	        isFired = false;

	        return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			if (!isFired) {
				calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
				String date = dateFormat.format(calendar.getTime());
				String dateThai = DateHelper.toThaiStr(date);
				dateEt.setText(dateThai);
				buildDataTable();
				Log.e("date", "selected");
				isFired = true;
			}
		}
		
	}*/
	
	
	private void createWebPrintJob(WebView webView) {

	    // Get a PrintManager instance
	    PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);

	    // Get a print adapter instance
	    PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();

	    // Create a print job with name and adapter instance
	    String jobName = getString(R.string.app_name) + " : Patient Report";
	    printManager.print(jobName, printAdapter,
	            new PrintAttributes.Builder().setMediaSize(new PrintAttributes.MediaSize("A4-LS", "A4 landscape", 210, 297).asLandscape()).build());

	    // Save the job object for later status checking
	    //mPrintJobs.add(printJob);
	}
}
