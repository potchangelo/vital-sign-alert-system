package com.kiwilauncher.vitalsignalertsystem;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class PatientAutocompleteAdapter extends ArrayAdapter<Patient> implements Filterable {
	private Context context;
	ModelController modelController;
	private SimpleDateFormat dateFormat;
	
	private List<Patient> patients;
	//private List<Patient> patientsOrg;
	
	private int layoutId;

	@SuppressLint("SimpleDateFormat")
	public PatientAutocompleteAdapter(Context context, int resource,
			List<Patient> objects, ModelController modelController) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.modelController = modelController;
		this.layoutId = resource;
		this.patients = objects;
		//this.patientsOrg = objects;
		this.dateFormat = new SimpleDateFormat(Patient.DATE_FORMAT);
		this.dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if (patients != null) {
			return patients.size();
		}
		else {
			return 0;
		}
	}

	@Override
	public Patient getItem(int position) {
		// TODO Auto-generated method stub
		return patients.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layoutId, parent, false);
        }
		
		Patient patient = patients.get(position);
		
		TextView atcpT = (TextView) convertView.findViewById(R.id.input_atcp_t);
		String birthdayStr = dateFormat.format(patient.getBirthdayDate());
		String yearStr = birthdayStr.substring(birthdayStr.lastIndexOf("/") + 1);
		int year = Integer.parseInt(yearStr) + 543;
		String birthdayStrThai = birthdayStr.replace(yearStr, "" + year);
		atcpT.setText(patient.getName() + " | " + birthdayStrThai);
		
		return convertView;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		Filter filter = new Filter() {

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				// TODO Auto-generated method stub
				FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                	/*List<Patient> resultPatients = new ArrayList<Patient>();
                	if (patientsOrg != null && patientsOrg.size() > 0) {
                		for (Patient patient : patientsOrg) {
                			if (patient.getName().contains(constraint)) {
                				resultPatients.add(patient);
                			}
                		}
                	}*/
                	patients = modelController.getPatients(constraint);
                	//Log.e("autocomplete", "patients count = " + patients.size());

                    // Assign the data to the FilterResults
                    filterResults.values = patients;
                    filterResults.count = patients.size();
                }

				return filterResults;
			}

			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				// TODO Auto-generated method stub
				if (results != null && results.count > 0) {
					notifyDataSetChanged();
	            } else {
	                notifyDataSetInvalidated();
	            }
			}
			
			@Override
			public CharSequence convertResultToString(Object resultValue) {
				// TODO Auto-generated method stub
				return ((Patient) resultValue).getName();
			}
		};
		
		return filter;
	}
}
