package com.kiwilauncher.vitalsignalertsystem;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;


public class PatientDataActivity extends Activity {
    private ImageButton headerBackBt;
    private TableLayout table;

    private SimpleDateFormat dateFormat, timeFormat;

    private ModelController modelController;

    private BuildDataTableTask buildTableTask;

    private Patient patient;

    private final String TIME_FORMAT = "HH:mm";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_data);


        // --------------------------Set Header functions--------------------------
        TextView headerT = (TextView) findViewById(R.id.header_t_title);
        headerT.setText("ข้อมูลผู้ป่วย");

        headerBackBt = (ImageButton) findViewById(R.id.header_imgbt_back);
        headerBackBt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setResult(1);
                finish();
            }
        });


        // --------------------------Set Models & Controllers--------------------------
        modelController = new ModelController(getApplicationContext());
        modelController.open();

        dateFormat = new SimpleDateFormat(Patient.DATE_FORMAT);
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
        timeFormat = new SimpleDateFormat(TIME_FORMAT);
        timeFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));


        // Set Views
        table = (TableLayout) findViewById(R.id.patient_data_table);


        // --------------------------Set Data from AsyncTask--------------------------
        buildTableTask = new BuildDataTableTask();
        buildTableTask.execute();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        modelController.close();

        if (buildTableTask.getStatus() == AsyncTask.Status.RUNNING) {
            buildTableTask.cancel(true);
        }

        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            buildTableTask = new BuildDataTableTask();
            buildTableTask.execute();
        }
    }


    private class BuildDataTableTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... params) {
            patient = modelController.getPatientWithPatientData(getIntent().getLongExtra("patient_id", 0));

            return 1;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            // --------------------------Set Views--------------------------
            table.removeViews(1, table.getChildCount() -1);

            TextView nameT = (TextView) findViewById(R.id.patient_data_tv_name);
            nameT.setText("ชื่อ-นามสกุล : " + patient.getName());

            TextView roomT = (TextView) findViewById(R.id.patient_data_tv_room);
            roomT.setText("ห้อง/เตียง : " + patient.getRoom());

            TextView typeT = (TextView) findViewById(R.id.patient_data_tv_type);
            typeT.setText("ประเภทผู้ป่วย : " + patient.getType());

            TextView hnT = (TextView) findViewById(R.id.patient_data_tv_hn);
            hnT.setText("HN : " + patient.getHn());

            TextView birthdayT = (TextView) findViewById(R.id.patient_data_tv_birthday);
            birthdayT.setText("วันเกิด : " + DateHelper.toThaiStr(dateFormat.format(patient.getBirthdayDate())));

            if (patient.getPatientDataList() != null) {
                int count = 0;

                for (PatientData pd : patient.getPatientDataList()) {
                    count++;

                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                    TableRow tr = (TableRow) inflater.inflate(R.layout.tablerow_patient_data, null);

                    TextView datetimeT = (TextView) tr.findViewById(R.id.patient_data_td_datetime);
                    datetimeT.setText(DateHelper.toThaiStr(dateFormat.format(pd.getCreatedAtDate())) + " " + timeFormat.format(pd.getCreatedAtDate()));

                    // Score info
                    TextView scoreT = (TextView) tr.findViewById(R.id.patient_data_td_score);
                    scoreT.setText("" + pd.getScore());
                    TextView pT = (TextView) tr.findViewById(R.id.patient_data_td_p);
                    pT.setText("" + pd.getP());
                    TextView rT = (TextView) tr.findViewById(R.id.patient_data_td_r);
                    rT.setText("" + pd.getR());
                    TextView bpT = (TextView) tr.findViewById(R.id.patient_data_td_bp);
                    bpT.setText("" + pd.getBp() + "/" + pd.getBpLower());
                    TextView spo2T = (TextView) tr.findViewById(R.id.patient_data_td_spo2);
                    spo2T.setText("" + pd.getSpo2());
                    
                    // Other info
                    TextView tT = (TextView) tr.findViewById(R.id.patient_data_td_t);
                    tT.setText("" + pd.getTStr());
                    TextView oT = (TextView) tr.findViewById(R.id.patient_data_td_o);
                    oT.setText("" + pd.getOStr());
                    TextView vT = (TextView) tr.findViewById(R.id.patient_data_td_v);
                    vT.setText("" + pd.getVStr());
                    TextView uT = (TextView) tr.findViewById(R.id.patient_data_td_u);
                    uT.setText("" + pd.getUStr());
                    TextView sT = (TextView) tr.findViewById(R.id.patient_data_td_s);
                    sT.setText("" + pd.getSStr());

                    // Set color
                    if (pd.getScore() > 2) {
                        scoreT.setTextColor(getStateColor(pd.getResultState()));
                    }
                    if (pd.getpScore() > 0) {
                        pT.setTextColor(getStateColor(pd.getpState()));
                    }
                    if (pd.getrScore() > 0) {
                        rT.setTextColor(getStateColor(pd.getrState()));
                    }
                    if (pd.getBpScore() > 0) {
                        bpT.setTextColor(getStateColor(pd.getBpState()));
                    }
                    if (pd.getSpo2Score() > 0) {
                        spo2T.setTextColor(getStateColor(pd.getSpo2State()));
                    }

                    // Set action
                    TextView editT = (TextView) tr.findViewById(R.id.patient_data_td_edit);
                    editT.setTag(pd);
                    editT.setPaintFlags(nameT.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    editT.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PatientData patient_data = (PatientData) v.getTag();
                            Intent editPatientDataAct = new Intent(getApplicationContext(), EditPatientDataActivity.class);
                            editPatientDataAct.putExtra("patient_data_id", patient_data.getId());
                            startActivityForResult(editPatientDataAct, 1);
                        }
                    });

                    if (count%2 == 0) {
                        tr.setBackgroundColor(getResources().getColor(R.color.almost_white));
                    }

                    table.addView(tr);
                }
            }

            super.onPostExecute(integer);
        }
    }


    private int getStateColor(int state) {
        int color;
        switch (state) {
            case PatientData.NORMAL_STATE:
                color = getResources().getColor(R.color.normal_state);
                break;
            case PatientData.WARNING_STATE:
                color = getResources().getColor(R.color.warning_state);
                break;
            case PatientData.ALERT_STATE:
                color = getResources().getColor(R.color.alert_state);
                break;
            default:
                color = getResources().getColor(R.color.black);
                break;
        }

        return color;
    }
}
