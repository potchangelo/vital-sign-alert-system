package com.kiwilauncher.vitalsignalertsystem;

import com.kiwilauncher.vitalsignalertsystem.Patient.PatientEntry;
import com.kiwilauncher.vitalsignalertsystem.PatientData.PatientDataEntry;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String DB_NAME = "vsas_database";
	private static final int DB_VERSION = 1;
	
	private static final String CREATE_TABLE_PATIENTS = "CREATE TABLE " + PatientEntry.TABLE_NAME + " ("
			+ PatientEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ PatientEntry.COLUMN_NAME + " TEXT NOT NULL, "
			+ PatientEntry.COLUMN_BIRTHDAY + " TIMESTAMP NOT NULL, "
			+ PatientEntry.COLUMN_ROOM + " TEXT NOT NULL, "
			+ PatientEntry.COLUMN_TYPE + " TEXT NOT NULL, "
			+ PatientEntry.COLUMN_HN + " TEXT, "
			+ PatientEntry.COLUMN_CREATED_AT + " TIMESTAMP NOT NULL, "
			+ PatientEntry.COLUMN_UPDATED_AT + " TIMESTAMP NOT NULL);";
	
	private static final String CREATE_TABLE_PATIENTDATA = "CREATE TABLE " + PatientDataEntry.TABLE_NAME + " ("
			+ PatientDataEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ PatientDataEntry.COLUMN_PATIENT_ID + " INTEGER NOT NULL, "
			+ PatientDataEntry.COLUMN_P + " INTEGER NOT NULL, "
			+ PatientDataEntry.COLUMN_R + " INTEGER NOT NULL, "
			+ PatientDataEntry.COLUMN_BP + " INTEGER NOT NULL, "
			+ PatientDataEntry.COLUMN_BP_LOWER + " INTEGER NOT NULL, "
			+ PatientDataEntry.COLUMN_SPO2 + " INTEGER NOT NULL, "
			+ PatientDataEntry.COLUMN_T + " FLOAT, "
			+ PatientDataEntry.COLUMN_O + " INTEGER, "
			+ PatientDataEntry.COLUMN_V + " INTEGER, "
			+ PatientDataEntry.COLUMN_U + " INTEGER, "
			+ PatientDataEntry.COLUMN_S + " INTEGER, "
			+ PatientDataEntry.COLUMN_CREATED_AT + " TIMESTAMP NOT NULL, "
			+ PatientDataEntry.COLUMN_UPDATED_AT + " TIMESTAMP NOT NULL);";

	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE_PATIENTS);
		db.execSQL(CREATE_TABLE_PATIENTDATA);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
