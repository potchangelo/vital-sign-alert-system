package com.kiwilauncher.vitalsignalertsystem;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ScoreActivity extends Activity {
	Button newBt, homeBt;
	ImageButton headerBackBt;
	TextView pT, rT, bpT, spo2T;
	TextView sumargsT, resultT, otherDataT;
	ImageView resultImg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_score);
		
		TextView headerT = (TextView) findViewById(R.id.header_t_title);
		headerT.setText("สรุปผลคะแนน");
		
		Intent input = getIntent();
		
		// display arguments for score
		int p = input.getIntExtra("p", 0);
		int r = input.getIntExtra("r", 0);
		String bp = input.getStringExtra("bp");
		int spo2 = input.getIntExtra("spo2", 0);
		
		int pScore = input.getIntExtra("pScore", 0);
		int rScore = input.getIntExtra("rScore", 0);
		int bpScore = input.getIntExtra("bpScore", 0);
		int spo2Score = input.getIntExtra("spo2Score", 0);
		
		int pState = input.getIntExtra("pState", R.color.black);
		int rState = input.getIntExtra("rState", R.color.black);
		int bpState = input.getIntExtra("bpState", R.color.black);
		int spo2State = input.getIntExtra("spo2State", R.color.black);
		
		pT = (TextView) findViewById(R.id.score_t_p);
		rT = (TextView) findViewById(R.id.score_t_r);
		bpT = (TextView) findViewById(R.id.score_t_bp);
		spo2T = (TextView) findViewById(R.id.score_t_spo2);
		pT.setText(getResources().getString(R.string.var_p_full) + " = " + p);
		pT.setTextColor(getStateColor(pState));
		rT.setText(getResources().getString(R.string.var_r_full) + " = " + r);
		rT.setTextColor(getStateColor(rState));
		bpT.setText(getResources().getString(R.string.var_bp_full) + " = " + bp);
		bpT.setTextColor(getStateColor(bpState));
		spo2T.setText(getResources().getString(R.string.var_spo2_full) + " = " + spo2);
		spo2T.setTextColor(getStateColor(spo2State));
		
		TextView pScoreT = (TextView) findViewById(R.id.score_t_p_score);
		pScoreT.setText(pScore + " คะแนน");
		
		TextView rScoreT = (TextView) findViewById(R.id.score_t_r_score);
		rScoreT.setText(rScore + " คะแนน");
		
		TextView bpScoreT = (TextView) findViewById(R.id.score_t_bp_score);
		bpScoreT.setText(bpScore + " คะแนน");
		
		TextView spo2ScoreT = (TextView) findViewById(R.id.score_t_spo2_score);
		spo2ScoreT.setText(spo2Score + " คะแนน");
		
		// display score
		int score = input.getIntExtra("score", 0);
		
		sumargsT = (TextView) findViewById(R.id.score_t_sumargs);
		sumargsT.setText("คะแนนรวม = " + score);
		
		// display result
		String resultStr = input.getStringExtra("resultStr");
		int resultState = input.getIntExtra("resultState", 0);
		
		resultT = (TextView) findViewById(R.id.score_t_result);
		resultT.setText(resultStr);
		resultT.setTextColor(getStateColor(resultState));
		
		// display image
		resultImg = (ImageView) findViewById(R.id.score_img_result);
		resultImg.setImageDrawable(getStateImg(resultState));
		
		// display other data
		String other = input.getStringExtra("other");
		
		otherDataT = (TextView) findViewById(R.id.score_t_other_data);
		otherDataT.setText(other);
		
		headerBackBt = (ImageButton) findViewById(R.id.header_imgbt_back);
		headerBackBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent inputAct = new Intent(getApplicationContext(), InputActivity.class);
				inputAct.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(inputAct);
			}
		});
		
		newBt = (Button) findViewById(R.id.score_bt_new);
		newBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent inputAct = new Intent(getApplicationContext(), InputActivity.class);
				inputAct.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(inputAct);
			}
		});
		
		homeBt = (Button) findViewById(R.id.score_bt_home);
		homeBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent startAct = new Intent(getApplicationContext(), StartActivity.class);
				startAct.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(startAct);
			}
		});
		
		if (score>=3 && score<=4) {
			MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.alert_001);
			mp.setLooping(false);
			mp.start();
		}
		else if (score>=5) {
			MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.alert_003);
			mp.setLooping(false);
			mp.start();
		}
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent startAct = new Intent(getApplicationContext(), StartActivity.class);
		startAct.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(startAct);
	}
	
	
	private int getStateColor(int state) {
		int color;
		switch (state) {
			case PatientData.NORMAL_STATE:
				color = getResources().getColor(R.color.normal_state);
				break;
			case PatientData.WARNING_STATE:
				color = getResources().getColor(R.color.warning_state);
				break;
			case PatientData.ALERT_STATE:
				color = getResources().getColor(R.color.alert_state);
				break;
			default:
				color = getResources().getColor(R.color.black);
				break;
		}
		
		return color;
	}
	
	private Drawable getStateImg(int state) {
		Drawable resultImg;
		switch (state) {
			case PatientData.NORMAL_STATE:
				resultImg = getResources().getDrawable(R.drawable.normal_state);
				break;
			case PatientData.WARNING_STATE:
				resultImg = getResources().getDrawable(R.drawable.warning_state);
				break;
			case PatientData.ALERT_STATE:
				resultImg = getResources().getDrawable(R.drawable.alert_state);
				break;
			default:
				resultImg = getResources().getDrawable(R.drawable.warning_state);
				break;
		}
		
		return resultImg;
	}
}
