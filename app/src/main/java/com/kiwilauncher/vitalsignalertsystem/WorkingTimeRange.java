package com.kiwilauncher.vitalsignalertsystem;

import java.util.ArrayList;
import java.util.Calendar;

public class WorkingTimeRange {
	private ArrayList<Integer> lowerHour, lowerMinute, upperHour, upperMinute;
	private int currentIndex;
	
	public WorkingTimeRange(int currentIndex) {
		// Set lower time
		lowerHour = new ArrayList<Integer>();
		lowerMinute = new ArrayList<Integer>();
		// 1
		lowerHour.add(0);
		lowerMinute.add(0);
		// 2
		lowerHour.add(8);
		lowerMinute.add(0);
		// 3
		lowerHour.add(16);
		lowerMinute.add(0);
		
		// Set upper time
		upperHour = new ArrayList<Integer>();
		upperMinute = new ArrayList<Integer>();
		// 1
		upperHour.add(8);
		upperMinute.add(0);
		// 2
		upperHour.add(16);
		upperMinute.add(0);
		// 3
		upperHour.add(24);
		upperMinute.add(0);
		
		this.currentIndex = currentIndex;
	}

	public int getSelectedLowerHour() {
		return lowerHour.get(currentIndex);
	}

	public int getSelectedLowerMinute() {
		return lowerMinute.get(currentIndex);
	}

	public int getSelectedUpperHour() {
		return upperHour.get(currentIndex);
	}

	public int getSelectedUpperMinute() {
		return upperMinute.get(currentIndex);
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	
	public int getIndex(Calendar calendarNow) {
		int index = 0;
		int hour = calendarNow.get(Calendar.HOUR_OF_DAY);
		for (int i=0; i<3; i++) {
			if (hour >= lowerHour.get(i) && hour < upperHour.get(i)) {
				index = i;
				break;
			}
		}
		
		return index;
	}
}
