package com.kiwilauncher.vitalsignalertsystem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.provider.BaseColumns;


public class Patient {
	private long id;
	private String name;
	private Calendar birthday;
	private String room;
	private String type;
	private String hn;
	
	private Calendar createdAt;
	private Calendar updatedAt;

	private List<PatientData> patientDataList;
	
	public static abstract class PatientEntry implements BaseColumns {
		public static final String TABLE_NAME = "patients";
		public static final String COLUMN_NAME = "name", COLUMN_BIRTHDAY = "birthday", 
				COLUMN_ROOM = "room", COLUMN_TYPE = "type", COLUMN_HN = "hn", 
				COLUMN_CREATED_AT = "created_at", COLUMN_UPDATED_AT = "updated_at";
	}
	
	public static final String DATE_FORMAT = "d/M/yyyy";

	public Patient(String name, Calendar birthday, String room, String type, String hn) {
		super();
		this.name = name;
		this.birthday = birthday;
		this.room = room;
		this.type = type;
		this.hn = hn;
		createdAt = new GregorianCalendar();
		updatedAt = new GregorianCalendar();
	}
	
	public Patient(String name, String birthdayStr, String room, String type, String hn) {
		super();
		this.name = name;
		setBirthday(birthdayStr);
		this.room = room;
		this.type = type;
		this.hn = hn;
		createdAt = new GregorianCalendar();
		updatedAt = new GregorianCalendar();
	}
	
	public Patient(long id, String name, Calendar birthday, String room, String type, String hn) {
		super();
		this.id = id;
		this.name = name;
		this.birthday = birthday;
		this.room = room;
		this.type = type;
		this.hn = hn;
		createdAt = new GregorianCalendar();
		updatedAt = new GregorianCalendar();
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Calendar getBirthday() {
		return birthday;
	}
	
	public Date getBirthdayDate() {
		return birthday.getTime();
	}

	public void setBirthday(Calendar birthday) {
		this.birthday = birthday;
	}
	
	@SuppressLint("SimpleDateFormat")
	public void setBirthday(String birthdayStr) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		try {
			Date date = dateFormat.parse(birthdayStr);
			this.birthday = new GregorianCalendar();
			this.birthday.setTime(date);
		} catch (ParseException e) {
			this.birthday = new GregorianCalendar();
			e.printStackTrace();
		}
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getHn() {
		return hn;
	}

	public void setHn(String hn) {
		this.hn = hn;
	}

	public Calendar getCreatedAt() {
		return createdAt;
	}
	
	public Date getCreatedAtDate() {
		return createdAt.getTime();
	}

	public void setCreatedAt(Calendar createdAt) {
		this.createdAt = createdAt;
	}

	public Calendar getUpdatedAt() {
		return updatedAt;
	}
	
	public Date getUpdatedAtDate() {
		return updatedAt.getTime();
	}

	public void setUpdatedAt(Calendar updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<PatientData> getPatientDataList() {
		return patientDataList;
	}

	public void setPatientDataList(List<PatientData> patientDataList) {
		this.patientDataList = patientDataList;
	}
}
