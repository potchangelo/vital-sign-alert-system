package com.kiwilauncher.vitalsignalertsystem;

public class DateHelper {
	public static String toThaiStr (String dateStr) {
		String yearStr = dateStr.substring(dateStr.lastIndexOf("/") + 1);
		int year = Integer.parseInt(yearStr) + 543;
		String dateStrThai = dateStr.replace(yearStr, "" + year);
		
		return dateStrThai;
	}
	
	public static String fromThaiStr (String dateStrThai) {
		String yearStr = dateStrThai.substring(dateStrThai.lastIndexOf("/") + 1);
		int year = Integer.parseInt(yearStr) - 543;
		String dateStr = dateStrThai.replace(yearStr, "" + year);
		
		return dateStr;
	}
}
