package com.kiwilauncher.vitalsignalertsystem;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class AboutActivity extends Activity {
	ImageButton headerBackBt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		
		TextView headerT = (TextView) findViewById(R.id.header_t_title);
		headerT.setText("About Application");
		
		TextView freesfxT = (TextView) findViewById(R.id.about_t_freesfxlink);
		freesfxT.setMovementMethod(LinkMovementMethod.getInstance());
		
		ImageView kiwilauncherImg = (ImageView) findViewById(R.id.about_img_kiwilauncher);
		kiwilauncherImg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
		        intent.setAction(Intent.ACTION_VIEW);
		        intent.addCategory(Intent.CATEGORY_BROWSABLE);
		        intent.setData(Uri.parse("http://www.kiwilauncher.com"));
		        startActivity(intent);
			}
		});
		
		headerBackBt = (ImageButton) findViewById(R.id.header_imgbt_back);
		headerBackBt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
}
