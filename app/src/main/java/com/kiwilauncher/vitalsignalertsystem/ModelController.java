package com.kiwilauncher.vitalsignalertsystem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kiwilauncher.vitalsignalertsystem.Patient.PatientEntry;
import com.kiwilauncher.vitalsignalertsystem.PatientData.PatientDataEntry;

public class ModelController {
	private DatabaseHelper dbHelper;
	private SQLiteDatabase db;
	
	public ModelController(Context context) {
		dbHelper = new DatabaseHelper(context);
	}
	
	public void open() {
		db = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	
	// -------------------------------- Patient --------------------------------
	public List<Patient> getAllPatients() {
		// TODO Auto-generated method stub
		List<Patient> patients = new ArrayList<Patient>();
		Cursor cursor = db.query(PatientEntry.TABLE_NAME ,null, null, null, null, null, null);
		cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	patients.add(cursorToPatient(cursor));
	    	cursor.moveToNext();
	    }
	    cursor.close();
		
		return patients;
	}

	public List<Patient> getRecentPatients() {
		List<Patient> patients = new ArrayList<Patient>();

		Cursor cursor = db.query(PatientEntry.TABLE_NAME ,null, null, null, null, null, PatientEntry.COLUMN_UPDATED_AT + " DESC", "30");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			patients.add(cursorToPatientWithLastPatientData(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		return patients;
	}

	public List<Patient> getRecentPatients(String name) {
		List<Patient> patients = new ArrayList<Patient>();

		String where = PatientEntry.COLUMN_NAME + " LIKE '%" + name + "%'";
		String orderBy = PatientEntry.COLUMN_UPDATED_AT + " DESC";

		Cursor cursor = db.query(PatientEntry.TABLE_NAME ,null, where, null, null, null, orderBy);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			patients.add(cursorToPatientWithLastPatientData(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		return patients;
	}

	public List<Patient> getRecentPatients(String name, Calendar calendar, int lowerHour, int lowerMinute, int upperHour, int upperMinute) {
		List<Patient> patients = new ArrayList<Patient>();

		long dateMs = calendar.getTime().getTime();
		long lowerMs = (((lowerHour * 60) * 60) + (lowerMinute * 60)) * 1000;
		long upperMs = (((upperHour * 60) * 60) + (upperMinute * 60)) * 1000;
		long lowerDateMs = dateMs + lowerMs;
		long upperDateMs = dateMs + upperMs;

		String where = PatientEntry.COLUMN_NAME + " LIKE '%" + name + "%' AND " +
				PatientEntry.COLUMN_UPDATED_AT + " >= " + lowerDateMs + " AND " +
				PatientEntry.COLUMN_UPDATED_AT + " < " + upperDateMs;
		String orderBy = PatientEntry.COLUMN_UPDATED_AT + " DESC";
		String limit = "30";

		Cursor cursor = db.query(PatientEntry.TABLE_NAME ,null, where, null, null, null, orderBy, limit);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			patients.add(cursorToPatientWithLastPatientData(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		return patients;
	}
	
	public List<Patient> getPatients(CharSequence name) {
		// TODO Auto-generated method stub
		List<Patient> patients = new ArrayList<Patient>();
		Cursor cursor = db.query(PatientEntry.TABLE_NAME ,null, PatientEntry.COLUMN_NAME + " LIKE '%" + name + "%'", null, null, null, null);
		cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	patients.add(cursorToPatient(cursor));
	    	cursor.moveToNext();
	    }
	    cursor.close();
		
		return patients;
	}
	
	public Patient getPatient(long id) {
		Cursor cursor = db.query(PatientEntry.TABLE_NAME ,null, PatientEntry._ID + " = " + id , null, null, null, null);
		if (cursor.getCount() == 0) {
			return null;
		}
		cursor.moveToFirst();
		Patient patient = cursorToPatient(cursor);
		cursor.close();
		
		return patient;
	}
	
	public Patient getPatient(String name, Calendar birthday) {
		String where = PatientEntry.COLUMN_NAME + " = '" + name + "' AND " + PatientEntry.COLUMN_BIRTHDAY + " = " + birthday.getTime().getTime();
		Cursor cursor = db.query(PatientEntry.TABLE_NAME ,null, where , null, null, null, null);
		if (cursor.getCount() == 0) {
			return null;
		}
		cursor.moveToFirst();
		Patient patient = cursorToPatient(cursor);
		cursor.close();
		
		return patient;
	}

	public Patient getPatientWithPatientData(long id) {
		Cursor cursor = db.query(PatientEntry.TABLE_NAME ,null, PatientEntry._ID + " = " + id , null, null, null, null);
		if (cursor.getCount() == 0) {
			return null;
		}
		cursor.moveToFirst();
		Patient patient = cursorToPatientWithPatientData(cursor);
		cursor.close();

		return patient;
	}

	public Patient createPatient(Patient patient) {
		// TODO Auto-generated method stub
		ContentValues values = patientToCvForCreate(patient);
		long insertId = db.insert(PatientEntry.TABLE_NAME, null, values);
		Cursor cursor = db.query(PatientEntry.TABLE_NAME, null, PatientEntry._ID + " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		Patient savedPatient = cursorToPatient(cursor);
		cursor.close();
		
		return savedPatient;
	}

	public Patient updatePatient(Patient patient) {
		// TODO Auto-generated method stub
		ContentValues values = patientToCvForUpdate(patient);
		db.update(PatientEntry.TABLE_NAME, values, PatientEntry._ID + " = " + patient.getId(), null);
		Cursor cursor = db.query(PatientEntry.TABLE_NAME, null, PatientEntry._ID + " = " + patient.getId(), null, null, null, null);
		cursor.moveToFirst();
		Patient savedPatient = cursorToPatient(cursor);
		cursor.close();

		return savedPatient;
	}

	public void deletePatients(List<Object> checkedPatientsId) {
		// TODO Auto-generated method stub
		StringBuilder idSetStr = new StringBuilder("(");
		for (int i = 0; i < checkedPatientsId.size(); i++) {
			int id = Integer.parseInt(checkedPatientsId.get(i).toString());
			if (i == 0) idSetStr.append("" + id);
			else idSetStr.append(", " + id);
		}
		idSetStr.append(")");
		String where = PatientEntry._ID + " in " + idSetStr.toString();
		String whereRel = PatientDataEntry.COLUMN_PATIENT_ID + " in " + idSetStr.toString();

		db.delete(PatientEntry.TABLE_NAME, where, null);
		db.delete(PatientDataEntry.TABLE_NAME, whereRel, null);
	}
	

	private ContentValues patientToCvForCreate(Patient patient) {
		// TODO Auto-generated method stub
		ContentValues values = new ContentValues();
		values.put(PatientEntry.COLUMN_NAME, patient.getName());
		values.put(PatientEntry.COLUMN_BIRTHDAY, patient.getBirthdayDate().getTime());
		values.put(PatientEntry.COLUMN_ROOM, patient.getRoom());
		values.put(PatientEntry.COLUMN_TYPE, patient.getType());
		values.put(PatientEntry.COLUMN_HN, patient.getHn());
		values.put(PatientEntry.COLUMN_CREATED_AT, patient.getCreatedAtDate().getTime());
		values.put(PatientEntry.COLUMN_UPDATED_AT, patient.getUpdatedAtDate().getTime());
		
		return values;
	}
	
	private ContentValues patientToCvForUpdate(Patient patient) {
		// TODO Auto-generated method stub
		ContentValues values = new ContentValues();
		values.put(PatientEntry.COLUMN_ROOM, patient.getRoom());
		values.put(PatientEntry.COLUMN_TYPE, patient.getType());
		values.put(PatientEntry.COLUMN_HN, patient.getHn());
		values.put(PatientEntry.COLUMN_UPDATED_AT, patient.getUpdatedAtDate().getTime());
		
		return values;
	}

	private Patient cursorToPatient(Cursor cursor) {
		// TODO Auto-generated method stub
		long id = cursor.getLong(cursor.getColumnIndex(PatientEntry._ID));
		String name = cursor.getString(cursor.getColumnIndex(PatientEntry.COLUMN_NAME));
		Calendar birthday = new GregorianCalendar();
		birthday.setTime(new Date(cursor.getLong(cursor.getColumnIndex(PatientEntry.COLUMN_BIRTHDAY))));
		String room = cursor.getString(cursor.getColumnIndex(PatientEntry.COLUMN_ROOM));
		String type = cursor.getString(cursor.getColumnIndex(PatientEntry.COLUMN_TYPE));
		String hn = cursor.getString(cursor.getColumnIndex(PatientEntry.COLUMN_HN));
		Calendar createdAt = new GregorianCalendar();
		createdAt.setTime(new Date(cursor.getLong(cursor.getColumnIndex(PatientEntry.COLUMN_CREATED_AT))));
		Calendar updatedAt = new GregorianCalendar();
		updatedAt.setTime(new Date(cursor.getLong(cursor.getColumnIndex(PatientEntry.COLUMN_UPDATED_AT))));
		
		Patient patient = new Patient(id, name, birthday, room, type, hn);
		patient.setCreatedAt(createdAt);
		patient.setUpdatedAt(updatedAt);
		
		return patient;
	}

	private Patient cursorToPatientWithPatientData(Cursor cursor) {
		// TODO Auto-generated method stub

		long id = cursor.getLong(cursor.getColumnIndex(PatientEntry._ID));
		String name = cursor.getString(cursor.getColumnIndex(PatientEntry.COLUMN_NAME));
		Calendar birthday = new GregorianCalendar();
		birthday.setTime(new Date(cursor.getLong(cursor.getColumnIndex(PatientEntry.COLUMN_BIRTHDAY))));
		String room = cursor.getString(cursor.getColumnIndex(PatientEntry.COLUMN_ROOM));
		String type = cursor.getString(cursor.getColumnIndex(PatientEntry.COLUMN_TYPE));
		String hn = cursor.getString(cursor.getColumnIndex(PatientEntry.COLUMN_HN));
		Calendar createdAt = new GregorianCalendar();
		createdAt.setTime(new Date(cursor.getLong(cursor.getColumnIndex(PatientEntry.COLUMN_CREATED_AT))));
		Calendar updatedAt = new GregorianCalendar();
		updatedAt.setTime(new Date(cursor.getLong(cursor.getColumnIndex(PatientEntry.COLUMN_UPDATED_AT))));

		Patient patient = new Patient(id, name, birthday, room, type, hn);
		patient.setCreatedAt(createdAt);
		patient.setUpdatedAt(updatedAt);

		List<PatientData> patientDataList = getPatientData(patient);
		patient.setPatientDataList(patientDataList);

		return patient;
	}

	private Patient cursorToPatientWithLastPatientData(Cursor cursor) {
		// TODO Auto-generated method stub

		long id = cursor.getLong(cursor.getColumnIndex(PatientEntry._ID));
		String name = cursor.getString(cursor.getColumnIndex(PatientEntry.COLUMN_NAME));
		Calendar birthday = new GregorianCalendar();
		birthday.setTime(new Date(cursor.getLong(cursor.getColumnIndex(PatientEntry.COLUMN_BIRTHDAY))));
		String room = cursor.getString(cursor.getColumnIndex(PatientEntry.COLUMN_ROOM));
		String type = cursor.getString(cursor.getColumnIndex(PatientEntry.COLUMN_TYPE));
		String hn = cursor.getString(cursor.getColumnIndex(PatientEntry.COLUMN_HN));
		Calendar createdAt = new GregorianCalendar();
		createdAt.setTime(new Date(cursor.getLong(cursor.getColumnIndex(PatientEntry.COLUMN_CREATED_AT))));
		Calendar updatedAt = new GregorianCalendar();
		updatedAt.setTime(new Date(cursor.getLong(cursor.getColumnIndex(PatientEntry.COLUMN_UPDATED_AT))));

		Patient patient = new Patient(id, name, birthday, room, type, hn);
		patient.setCreatedAt(createdAt);
		patient.setUpdatedAt(updatedAt);

		List<PatientData> patientDataList = getLastPatientData(patient);
		patient.setPatientDataList(patientDataList);

		return patient;
	}
	// -------------------------------- Patient --------------------------------
	
	
	// -------------------------------- Patient Data --------------------------------
	public List<Patient> getAllPatientData() {
		// TODO Auto-generated method stub
		List<Patient> patients = new ArrayList<Patient>();
		
		return patients;
	}

	public PatientData getPatientData(long id) {
		Cursor cursor = db.query(PatientDataEntry.TABLE_NAME ,null, PatientDataEntry._ID + " = " + id , null, null, null, null);
		if (cursor.getCount() == 0) {
			return null;
		}
		cursor.moveToFirst();
		PatientData patientData = cursorToPatientData(cursor);
		cursor.close();

		return patientData;
	}
	
	public List<PatientData> getPatientData(Calendar calendar, int lowerHour, int lowerMinute, int upperHour, int upperMinute) {
		// TODO Auto-generated method stub
		List<PatientData> patientData = new ArrayList<PatientData>();

		long dateMs = calendar.getTime().getTime();
		long lowerMs = (((lowerHour * 60) * 60) + (lowerMinute * 60)) * 1000;
		long upperMs = (((upperHour * 60) * 60) + (upperMinute * 60)) * 1000;
		long lowerDateMs = dateMs + lowerMs;
		long upperDateMs = dateMs + upperMs;

		//Log.e("get patient data", "lh = " + lowerHour + ", lm = " + lowerMinute);
		//Log.e("get patient data", "lower = " + lowerDateMs + ", upper = " + upperDateMs);
		
		String where = PatientDataEntry.COLUMN_CREATED_AT + " >= " + lowerDateMs + " AND " +
				PatientDataEntry.COLUMN_CREATED_AT + " < " + upperDateMs;
		
		Cursor cursor = db.query(PatientDataEntry.TABLE_NAME ,null, where, null, null, null, "created_at ASC");
		cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	patientData.add(cursorToPatientData(cursor));
	    	cursor.moveToNext();
	    }
	    cursor.close();
	    
	    Collections.sort(patientData, PatientData.ROOM_SORT);
		
		return patientData;
	}

	public List<PatientData> getPatientData(Patient patient) {
		List<PatientData> patientDataList = new ArrayList<PatientData>();
		Cursor cursor = db.query(PatientDataEntry.TABLE_NAME ,null, PatientDataEntry.COLUMN_PATIENT_ID + " = " + patient.getId() , null, null, null,
				PatientDataEntry.COLUMN_CREATED_AT + " DESC");
		if (cursor.getCount() == 0) {
			return null;
		}

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			patientDataList.add(cursorToPatientData(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		return patientDataList;
	}

	public List<PatientData> getLastPatientData(Patient patient) {
		List<PatientData> patientDataList = new ArrayList<PatientData>();
		Cursor cursor = db.query(PatientDataEntry.TABLE_NAME ,null, PatientDataEntry.COLUMN_PATIENT_ID + " = " + patient.getId() , null, null, null,
				PatientDataEntry.COLUMN_CREATED_AT + " DESC", "1");
		if (cursor.getCount() == 0) {
			return null;
		}

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			patientDataList.add(cursorToPatientData(cursor));
			cursor.moveToNext();
		}
		cursor.close();

		return patientDataList;
	}

	public PatientData createPatientData(PatientData patientData) {
		// TODO Auto-generated method stub
		ContentValues values = patientDataToCvForCreate(patientData);
		long insertId = db.insert(PatientDataEntry.TABLE_NAME, null, values);
		Cursor cursor = db.query(PatientDataEntry.TABLE_NAME, null, PatientEntry._ID + " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		PatientData savedPatientData = cursorToPatientData(cursor);
		cursor.close();
		
		return savedPatientData;
	}

	public PatientData updatePatientData(PatientData patientData) {
		// TODO Auto-generated method stub
		ContentValues values = patientDataToCvForUpdate(patientData);
		db.update(PatientDataEntry.TABLE_NAME, values, PatientDataEntry._ID + " = " + patientData.getId(), null);
		Cursor cursor = db.query(PatientDataEntry.TABLE_NAME, null, PatientDataEntry._ID + " = " + patientData.getId(), null, null, null, null);
		cursor.moveToFirst();
		PatientData savedPatientData = cursorToPatientData(cursor);
		cursor.close();

		return savedPatientData;
	}

	public void deletePatientData() {
		// TODO Auto-generated method stub
		
	}
	
	private ContentValues patientDataToCvForCreate(PatientData patientData) {
		// TODO Auto-generated method stub
		ContentValues values = new ContentValues();
		
		values.put(PatientDataEntry.COLUMN_PATIENT_ID, patientData.getPatient().getId());
		
		values.put(PatientDataEntry.COLUMN_P, patientData.getP());
		values.put(PatientDataEntry.COLUMN_R, patientData.getR());
		values.put(PatientDataEntry.COLUMN_BP, patientData.getBp());
		values.put(PatientDataEntry.COLUMN_BP_LOWER, patientData.getBpLower());
		values.put(PatientDataEntry.COLUMN_SPO2, patientData.getSpo2());
		
		if (patientData.getT() != null) {
			values.put(PatientDataEntry.COLUMN_T, patientData.getT());
		}
		if (patientData.getO() != null) {
			values.put(PatientDataEntry.COLUMN_O, patientData.getO());
		}
		if (patientData.getV() != null) {
			values.put(PatientDataEntry.COLUMN_V, patientData.getV());
		}
		if (patientData.getU() != null) {
			values.put(PatientDataEntry.COLUMN_U, patientData.getU());
		}
		if (patientData.getS() != null) {
			values.put(PatientDataEntry.COLUMN_S, patientData.getS());
		}
		
		values.put(PatientDataEntry.COLUMN_CREATED_AT, patientData.getCreatedAtDate().getTime());
		values.put(PatientDataEntry.COLUMN_UPDATED_AT, patientData.getUpdatedAtDate().getTime());
		
		return values;
	}

	private ContentValues patientDataToCvForUpdate(PatientData patientData) {
		// TODO Auto-generated method stub
		ContentValues values = new ContentValues();

		values.put(PatientDataEntry.COLUMN_P, patientData.getP());
		values.put(PatientDataEntry.COLUMN_R, patientData.getR());
		values.put(PatientDataEntry.COLUMN_BP, patientData.getBp());
		values.put(PatientDataEntry.COLUMN_BP_LOWER, patientData.getBpLower());
		values.put(PatientDataEntry.COLUMN_SPO2, patientData.getSpo2());

		if (patientData.getT() != null) {
			values.put(PatientDataEntry.COLUMN_T, patientData.getT());
		}
		if (patientData.getO() != null) {
			values.put(PatientDataEntry.COLUMN_O, patientData.getO());
		}
		if (patientData.getV() != null) {
			values.put(PatientDataEntry.COLUMN_V, patientData.getV());
		}
		if (patientData.getU() != null) {
			values.put(PatientDataEntry.COLUMN_U, patientData.getU());
		}
		if (patientData.getS() != null) {
			values.put(PatientDataEntry.COLUMN_S, patientData.getS());
		}

		values.put(PatientDataEntry.COLUMN_UPDATED_AT, patientData.getUpdatedAtDate().getTime());

		return values;
	}

	private PatientData cursorToPatientData(Cursor cursor) {
		// TODO Auto-generated method stub
		long id = cursor.getLong(cursor.getColumnIndex(PatientDataEntry._ID));
		int patiend_id = cursor.getInt(cursor.getColumnIndex(PatientDataEntry.COLUMN_PATIENT_ID));
		
		Patient patient = getPatient(patiend_id);
		
		int p = cursor.getInt(cursor.getColumnIndex(PatientDataEntry.COLUMN_P));
		int r = cursor.getInt(cursor.getColumnIndex(PatientDataEntry.COLUMN_R));
		int bp = cursor.getInt(cursor.getColumnIndex(PatientDataEntry.COLUMN_BP));
		int bpLower = cursor.getInt(cursor.getColumnIndex(PatientDataEntry.COLUMN_BP_LOWER));
		int spo2 = cursor.getInt(cursor.getColumnIndex(PatientDataEntry.COLUMN_SPO2));
		
		String t = null;
		String o = null, v = null, u = null, s = null;
		
		if (!cursor.isNull(cursor.getColumnIndex(PatientDataEntry.COLUMN_T))) {
			t = cursor.getString(cursor.getColumnIndex(PatientDataEntry.COLUMN_T));
		}
		if (!cursor.isNull(cursor.getColumnIndex(PatientDataEntry.COLUMN_O))) {
			o = cursor.getString(cursor.getColumnIndex(PatientDataEntry.COLUMN_O));
		}
		if (!cursor.isNull(cursor.getColumnIndex(PatientDataEntry.COLUMN_V))) {
			v = cursor.getString(cursor.getColumnIndex(PatientDataEntry.COLUMN_V));
		}
		if (!cursor.isNull(cursor.getColumnIndex(PatientDataEntry.COLUMN_U))) {
			u = cursor.getString(cursor.getColumnIndex(PatientDataEntry.COLUMN_U));
		}
		if (!cursor.isNull(cursor.getColumnIndex(PatientDataEntry.COLUMN_S))) {
			s = cursor.getString(cursor.getColumnIndex(PatientDataEntry.COLUMN_S));
		}
		
		//Log.e("cursor to pd", "created at = " + cursor.getLong(cursor.getColumnIndex(PatientEntry.COLUMN_CREATED_AT)));
		
		Calendar createdAt = new GregorianCalendar();
		createdAt.setTime(new Date(cursor.getLong(cursor.getColumnIndex(PatientDataEntry.COLUMN_CREATED_AT))));
		Calendar updatedAt = new GregorianCalendar();
		updatedAt.setTime(new Date(cursor.getLong(cursor.getColumnIndex(PatientDataEntry.COLUMN_UPDATED_AT))));
		
		PatientData patientData = new PatientData(patient, id, p, r, bp, bpLower, spo2);
		patientData.calculateScore();
		patientData.setOtherData(t, o, v, u, s);
		patientData.setCreatedAt(createdAt);
		patientData.setUpdatedAt(updatedAt);
		
		return patientData;
	}
	// -------------------------------- Patient Data --------------------------------
}
